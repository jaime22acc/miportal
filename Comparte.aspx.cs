﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
namespace Emplea
{
    public partial class Comparte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (System.Web.HttpContext.Current.Session["Usuario"] != null)
            //{

             
            ////    LogPortal();
            ////}
            ////else
            ////{
            ////    Response.Redirect("Login.aspx");
            ////}
        }


        private static string GetConnectionStringByName(string p)
        {
            string returnValue = null;
            System.Configuration.ConnectionStringSettings settings =
            System.Configuration.ConfigurationManager.ConnectionStrings[p];
            if (settings != null)
                returnValue = settings.ConnectionString;
            return returnValue;
        }

        private static void LogPortal()
        {

            string Usuario = System.Web.HttpContext.Current.Session["Usuario"].ToString();

            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPI_Por_Log", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Usuario", Usuario);
            cmd.Parameters.AddWithValue("@Pantalla", "Comparte");
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }



        }

    }
}