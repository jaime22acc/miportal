﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.Master" AutoEventWireup="true" CodeBehind="Comparte.aspx.cs" Inherits="Emplea.Comparte" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      
    <div class="content-nomina">
        <h4>Boletín Comparte</h4>
        <div class="border-small"></div>
        <br>

      <%--  <div class="main-container container--wide container">
            <div class="container">

                <table class="tablemanager table" id="Tabla">
                </table>
            </div>
        </div>--%>

        	<main class="main-container container container--wide m-b-30">
		<div class="block block-seccion-1 clearfix">

        <div class="block block-seccion-1 clearfix">
    <div class="container">
      <h1 class="title-seccion">Publicaciones 2022</h1>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-4">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\8_COMPARTE_JUNIO_A.pdf" target="_blank">
              <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Primera Edición de Junio</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\7_COMPARTE_MAYO_B.pdf" target="_blank" />
              <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Segunda Edicion Mayo
                </p>
               
              </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\6_COMPARTE_MAYO_A.pdf" target="_blank">
              <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Primera Edición Mayo</p>
               
              </div>
            </div>
          </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\5_COMPARTE_ABRIL_B.pdf" target="_blank">
             <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Segunda Edición Abril</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\4_COMPARTE_ABRIL_A.pdf" target="_blank">
              <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Primera Edición Abril</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\3_COMPARTE_MARZO_B.pdf" target="_blank">
               <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Segunda Edición Marzo</p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\2_COMPARTE_FEBRERO_B.pdf" target="_blank">
            <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Segunda Edición Febrero </p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Boletines\PDF\1_COMPARTE_FEBRERO_A.pdf" target="_blank">     
             <div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Primera Edición Febrero</p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
    </div>
</div>
                </main>







</asp:Content>
