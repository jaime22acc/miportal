﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;



namespace Emplea
{
    public partial class Perfil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        
        }

        private static void LogPortal()
        {

            string Usuario = System.Web.HttpContext.Current.Session["Usuario"].ToString();

            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPI_Por_Log", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Usuario", Usuario);
            cmd.Parameters.AddWithValue("@Pantalla", "Perfil");
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }



        }

        private static string GetConnectionStringByName(string p)
        {
            string returnValue = null;
            System.Configuration.ConnectionStringSettings settings =
            System.Configuration.ConfigurationManager.ConnectionStrings[p];
            if (settings != null)
                returnValue = settings.ConnectionString;
            return returnValue;
        }

     


        [WebMethod]
        public static EmpleadoResultado getDatosEmpleado()
        {
            EmpleadoResultado d = new EmpleadoResultado();
            string Usuario = HttpContext.Current.Session["Usuario"].ToString();
            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPQ_Bio_DatosGeneralesTrabajoGafete", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Gafete", Usuario);
            try
            {

                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    d.NoEmpleado = dr["IdEmpleado"].ToString();
                    d.Nombre = dr["Nombre"].ToString();
                    d.Secretaria = dr["Secretaria"].ToString();
                    d.Dependencia = dr["Presupuestal"].ToString();
                  
                    d.TipoEmpleado = dr["TipoContratacion"].ToString();
                  
                    d.Telefono = dr["telefono"].ToString();
                    
                    d.Extension = dr["EXT"].ToString();
                    d.Correo = dr["EmailInstitucional"].ToString();                   
                    d.LugarTrabajo = dr["CatLugarTrabajo"].ToString();

                    d.FechaIngreso = dr["FechaIngreso"].ToString();                   

                    d.Dispositivo = dr["Dispositivo"].ToString();
                    
                    dr.Close();
                }
                dr.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }

                return d;
        }


        [WebMethod]
        public static EmpleadoResultado Actualizar(string IdEmpleado, string Email, string Telefono, string Extension)

        {
            EmpleadoResultado d = new EmpleadoResultado();
           
            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPU_PerfilPortal", con);
            cmd.CommandType = CommandType.StoredProcedure;
           
            cmd.Parameters.AddWithValue("@IdEmpleado", IdEmpleado);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Telefono", Telefono);
            cmd.Parameters.AddWithValue("@Extension", Extension);
           
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    dr.Close();
                }
                dr.Dispose();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }


            return d;



        }




        public class EmpleadoResultado
        {
            public string NoEmpleado { get; set; }
            public string Nombre { get; set; }
            public string Secretaria { get; set; }
            public string Dependencia { get; set; }
            public string Correo { get; set; }
            public string Telefono { get; set; }
            public string Extension { get; set; }
            public string TipoEmpleado { get; set; }
          
            public string FechaIngreso { get; set; }            
            public string Dispositivo { get; set; }
            public string LugarTrabajo { get; set; }
        }


    }
}