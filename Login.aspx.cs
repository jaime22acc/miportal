﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.SessionState;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Web.Security;

namespace Emplea
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod(EnableSession = true)]
        public static string Logines(string UserName, string Password)
        {
            string itsOK = "OK";
            string iUsuario;
            // Path to you LDAP directory server.
            // Contact your network administrator to obtain a valid path.
            string adPath = "LDAP://10.144.0.0:100/";
            LdapAuthentication adAuth = new LdapAuthentication(adPath);

            try
            {
                if (true == adAuth.IsAuthenticated("genl",
                                                  UserName,
                                                  Password))
                {
                    //Validar si es usuario en la aplicación
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    System.Configuration.ConnectionStringSettings settings =
                    System.Configuration.ConfigurationManager.ConnectionStrings["cnnPlantilla"];

                    string conexion = settings.ConnectionString;
                    SqlConnection con = new SqlConnection(conexion);
                    SqlCommand cmd = new SqlCommand("SPQ_PermisoLogInPortal", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@usuario", UserName);
                    try
                    {
                        cmd.Connection.Open();
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (!dr.HasRows)
                        {
                            iUsuario = UserName;

                            itsOK = "Aún no tienes  permisos para esta aplicación. Comunícate con el administrador.";
                        }
                        else
                        {


                            dr.Read();

                            HttpContext.Current.Session["dependencia"] = dr["dependencia"].ToString();

                            dr.Close();

                            HttpContext.Current.Session["Usuario"] = UserName;



                            itsOK = "OK";
                        }
                        dr.Dispose();
                    }
                    catch (Exception ex)
                    {

                        itsOK = "Error de Autentificación. Revise su cuenta.";
                        throw ex;
                    }
                    finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }

                }
                else
                {
                    itsOK = "Error de Acceso, Revisa tu nombre de usuario y contraseña.";
                }
            }
            catch (Exception ex)
            {
                itsOK = "Error de Autentificación. Revisa tu nombre de usuario y contraseña.";
            }

            return itsOK;
        }

    }
}