﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Emplea.Default" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="Scripts/img/favicon.ico" type="image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="follow, index">
    <meta name="generator" content="Gobierno del Estado de Nuevo León">
    <title> Inicio | Gobierno del Estado de Nuevo León</title>
 	  <link href="Scripts/css/bootstrap.min.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/dropdowns/">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="Scripts/css/carousel.css" rel="stylesheet">

    <style>
/*    @import url("Scripts/css/bootstrap-grid.css");*/
    @import url("Scripts/css/style.css");
    </style>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap');
    </style>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700;800;900&display=swap');
    </style>
    <style>::-webkit-scrollbar {
      width: 8px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
    }
    
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    } @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");</style>

</head>

<body class="html page page-login home">
	<header id="navbar" class="navbar navbar-fixed-top navbar-default">
  	<div class="container-fluid">
      	<div class="navbar-header">
          <a class="logo navbar-btn pull-left" href="Default.aspx" title="Inicio">
          	<img src="Scripts/img/logo-header.svg" height="40" alt="Inicio">
          </a>
        </div>
        <div class="label-header">
            <div class="label">Este es un sitio web oficial del Gobierno del Estado de Nuevo León. </div>
          	<%--<div class="label">Este es un sitio web oficial del Gobierno del Estado de Nuevo León. <a href="#">Aprende a identificarlos</a></div>--%>
        </div>
    </div>
    <div class="box-linea"></div>
    <div class="container-fluid menu-main">
      <div class="col">
        <div class="title-header"> MI NUEVO PORTAL </div>
      </div>
      <div class="col-md-auto">
        <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0 menu-horizontal">
          <li><a href="Default.aspx" class="nav-link px-2 link-secondary activo">Inicio</a></li>
          <li><a href="https://nl.gob.mx/"  target="_blank" class="nav-link px-2 link-dark">Acerca de</a></li>
          <li><a href="Directorio.aspx"  target="_blank"  class  ="nav-link px-2 link-dark">Directorio</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Ayuda</a></li>
        </ul>
      </div>
        <div class="col col-lg-2">
            <div class="dropdown text-end">
                <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                  
                    <img src="Scripts/img/userBlue.jpg" alt="mdo" width="32" height="32" class="rounded-circle">
                </a>
                <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                    <div class="num-empleado">
                        <div class="num-empleado-label">
                            <p style="font-family: 'Century Gothic'" id="lblUsuario" runat="server"></p>
                        </div>
                        <div class="num-empleado-codigo">
                            <p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblDependencia" runat="server"></p>
                        </div>

                    </div>
                    <div class="usuario-perfil-controles">
                        <%--<button class="btn green">Activo</button>
                        <button class="btn brown">Editar Perfil</button>--%>
                    </div>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <li><a class="dropdown-item" href="perfil.aspx">
                        <img src="Scripts/img/Mi_cuenta.svg">
                        Mi Cuenta</a></li>
                    <li><a class="dropdown-item" href="Login.aspx">
                        <img src="Scripts/img/cerrar_sesion.svg">
                        Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
      

    </div>
	</header>


<%-- <div class="block block-seccion-2 clearfix">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="Scripts/img/Carrusel-Procesos eficientes.png" alt="Talento">
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="Scripts/img/Carrusel-Talento.png" alt="Colaboración Empresarial">
          </div>
         <div class="carousel-item">
            <img class="third-slide" src="Scripts/img/Procesos_eficientes.png" alt="Procesos eficientes">
          </div
          <div class="carousel-item">
            <img class="fourth-slide" src="Scripts/img/Carrusel-Tecnologia.png" alt="Tecnologia">
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>--%>
    <div class="banner-default d-flex flex-column justify-content-center align-items-center">
        <div class="text-center baner-title "> En Mi nuevo portal encuentra tus servicios de manera clara y simple</div>
        
        <div class="d-flex banner-width">
            <input class="banner-input form-control" placeholder="¿Que estas buscando?" type="text">
            <button class="btn-buscar">Buscar</button>
        </div>
    </div>


	<main class="main-container container container--wide m-b-30">
		<div class="block block-seccion-1 clearfix">
    <div class="container">
      <h1 class="title-seccion">Servicios disponibles</h1>
      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-4">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="NominaR.aspx">
            <div class="seccion-container">
                <img src="../Scripts/img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="../Scripts/img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Informacion personal</span>
                </div>
            </div>
            <%--<div class="card shadow-sm">
              <div class="img-servicios-disponibles nomina"></div>
              <div class="circulo-img img-nomina"></div>
              <div class="card-body">
                <p class="card-text">Recibo de Nómina</p>
              </div>
            </div>--%>
          </a>
        </div>
          <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Forma37.aspx">
            <%--<div class="card shadow-sm">            
              <div class="img-servicios-disponibles constancia_sueldos"></div>
              <div class="circulo-img img-sueldos-retenciones"></div>
              <div class="card-body">
                <p class="card-text">Constancia de sueldos y retenciones (Forma 37)</p>
              </div>
            </div>--%>
            <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>SOY el Nuevo Nuevo León</span>
                </div>
            </div>
          </a>
        </div>



  
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
            <%--<div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_a_retiro"></div>
              <div class="circulo-img img-f-ahorro-retiro"></div>
              <div class="card-body">
                <p class="card-text">Fondo de Ahorro para el retiro</p>
                <%--<div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
              <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Instituto de Profesionalización</span>
                </div>
            </div>
          </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="http://profesionalizacion.nl.gob.mx/index.html" target="_blank">
            <%--<div class="card shadow-sm">
              <div class="img-servicios-disponibles cursos_online"></div>
              <div class="circulo-img img-cursos-online"></div>
              <div class="card-body">
                <p class="card-text">Capacitación</p>
              </div>
            </div>--%>
              <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Recibos de nómina</span>
                </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Patrimonial.aspx">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles declaracion_patrimonial"></div>
              <div class="circulo-img img-declaracion-patrimonial"></div>
              <div class="card-body">
                <p class="card-text">Informe de ingresos para declaración patrimonial</p>
              </div>
            </div>--%>
            <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Fondo de reserva individualizada</span>
                </div>
            </div>
          </a>
        </div>
        
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Isssteleón</span>
                </div>
            </div>
          </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="Comparte.aspx">
            <%--<div class="card shadow-sm">
              <div class="img-servicios-disponibles boletin"></div>
              <div class="circulo-img img-boletin"></div>
              <div class="card-body">
                <p class="card-text">Boletín <br>‘Comparte’ </p>
              </div>
            </div>--%>
            <div class="seccion-container">
                <img src="../Scripts/img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Préstamo a colaboradores</span>
                </div>
            </div>
          </a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios/Vacacion.aspx" target="_blank">     
            <%--<div class="card shadow-sm">
              <div class="img-servicios-disponibles Vacaciones"></div>
              <div class="circulo-img img-vacaciones"></div>
              <div class="card-body">
                <p class="card-text">Vacaciones</p>
              </div>
            </div>--%>
            <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Apoyo a la educación</span>
                </div>
            </div>
          </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Constancias de nómina</span>
                </div>
            </div>
          </a>
        </div>

          <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Constancias de sueldos y retenciones </span>
                </div>
            </div>
          </a>
        </div>

          <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Código de ética</span>
                </div>
            </div>
          </a>
        </div>

          <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Declaración patrimonial</span>
                </div>
            </div>
          </a>
        </div>

          <div class="col-12 col-sm-6 col-md-4 col-lg-3 m-b-15">
          <a href="https://sa.nl.gob.mx/Rhservicios" target="_blank">
           <%-- <div class="card shadow-sm">
              <div class="img-servicios-disponibles fondo_ahorro"></div>
              <div class="circulo-img img-fondo-ahorro"></div>
              <div class="card-body">
                <p class="card-text">Apoyo Escolar Corporaciones Policiacas
                </p>
               <%-- <div class="subtitle">Próximamente</div>
              </div>
            </div>--%>
             <div class="seccion-container">
                <img src="../img/Declaracion_Patrimonial.png" alt="" class="">
                <div class="d-flex flex-column align-items-center mt-3">
                    <img src="img/Declaracion_Patrimonial.svg" class="seccion-icon" alt="">
                    <span>Décalogo de principios</span>
                </div>
            </div>
          </a>
        </div>

      </div>
    </div>
		</div>
	</main>
 
	<footer class="blue">
		<div class="row">
		    <div class="col-12 col-xl-2 col-md-12">
		      	<div class="region region-footer-logo">
				  	<img class="footer_img" src="Scripts/img/logo-escudo-footer.svg" alt="nuevo-leon" title="Nuevo León">
				</div>
		    </div>
		    <div class="col-12 col-xl-5 col-md-8 margin-auto">
		      <h3>Servicios para colaboradores</h3>
		      <div class="col-12 col-xl-6 col-md-6 izq">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="NominaR.aspx" title="">Recibo de Nómina</a></li>
					<li class="leaf"><a  href="https://sa.nl.gob.mx/Rhservicios" target="_blank" title="">Apoyo Escolar Corporaciones Policiacas</a></li>
					<li class="leaf"><a href="https://www.isssteleon.gob.mx/afiliado/solicitud_pcp.html" target="_blank" title="ISSSTELEON">Préstamos</a></li>
					<%--<li class="leaf"><a href="#" title="">Prestaciones</a></li>--%>
					<li class="leaf"><a href="https://sa.nl.gob.mx/Rhservicios/Vacacion.aspx" target="_blank" title="RHSERVICIOS">Vacaciones </a></li>
					<%--<li class="last"><a href="#" title="">Refrendo</a></li>--%>
				</ul>
		      </div>
		      <div class="col-12 col-xl-6 col-md-6 der">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="Forma37.aspx" title="">Sueldos y retenciones</a></li>
					<li class="leaf"><a href="Patrimonial.aspx" title="">Declaración patrimonial</a></li>
					<%--<li class="leaf"><a href="#" title="">Constancia laboral</a></li>--%>
					<li class="leaf"><a href="http://profesionalizacion.nl.gob.mx/index.html" target="_blank" title="">Capacitación</a></li>
					<%--<li class="leaf"><a href="#" title="">Un servicio más</a></li>
					<li class="last"><a href="#" title="">Otro más</a></li>--%>
				</ul>
		      </div>
		    </div>
		    <div class="col-12 col-xl-2 col-md-8 margin-auto">
		    <%--  	<h3>Atención y soporte</h3>--%>
		      	<div class="col-12 col-xl-12 der">
			      	<%--<p>¿Tienes alguna solicitud de mantenimiento o requieres apoyo tecnológico?</p>
			      	<p>Contáctanos y estaremos contigo para ayudarte</p>--%>
		      	</div>
		    </div>
		    <div class="col-12 col-xl-3 col-md-8 margin-auto">
		    	<h3></h3>
		    	<div class="col-12 col-xl-12">
		      		<img class="footer_img" src="Scripts/img/logo_administracion.png" alt="administracion" title="Administración">
		      	</div>
		    </div>
		    <div class="social der">
		    	<div class="der">
		    		<div class="facebook alineado"><a href="https://www.facebook.com/gobiernonuevoleon/"><img src="Scripts/img/Facebook.svg"></a></div>
		    		<div class="instagram alineado"><a href="https://www.instagram.com/nuevoleonmx"><img src="Scripts/img/Instagram.svg"></a></div>
		    		<div class="youtube alineado"><a href="https://www.youtube.com/user/GobiernoNuevoLeon"><img src="Scripts/img/Youtube.svg"></a></div>
		    	</div>
		    </div>
		    <div class="col-12 center">
		    	<h5>Gobierno del Estado de Nuevo León</h5>
		    </div>
		</div>
	</footer>
	<footer class="white">
		<div class="alineado text-dorado">SECRETARÍA DE ADMINISTRACIÓN</div>
		<div class="alineado"><img src="Scripts/img/footer_leon.svg"></div>
		<div class="alineado text-dorado">UN NUEVO NUEVO LEÓN</div>
	</footer>
  <!-- Placed at the end of the document so the pages load faster -->
  <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript'>document.addEventListener("DOMContentLoaded", function(event) {
   
        const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)

        // Validate that all variables exist
        if(toggle && nav && bodypd && headerpd){
        toggle.addEventListener('click', ()=>{
        // show navbar
        nav.classList.toggle('show')
        // change icon
        toggle.classList.toggle('bx-x')
        // add padding to body
        bodypd.classList.toggle('body-pd')
        // add padding to header
        headerpd.classList.toggle('body-pd')
        })
        }
        }

        showNavbar('header-toggle','nav-bar','body-pd','header')

        /*===== LINK ACTIVE =====*/
        const linkColor = document.querySelectorAll('.nav_link')

        function colorLink(){
        if(linkColor){
        linkColor.forEach(l=> l.classList.remove('active'))
        this.classList.add('active')
        }
        }
        linkColor.forEach(l=> l.addEventListener('click', colorLink))

         // Your code to run since DOM is loaded and ready
        });
    </script>
    <script type='text/javascript'>var myLink = document.querySelector('a[href="#"]');
    myLink.addEventListener('click', function(e) {
      e.preventDefault();
    });</script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="Scripts/js/popper.min.js"></script>
    <script src="Scripts/js/bootstrap.min.js"></script>
    <script src="Scripts/js/holder.min.js"></script>
</body>
</html>