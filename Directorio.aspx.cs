﻿ausing System;
using System.Text;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using RestSharp;
using System.Xml;

namespace Emplea
{
    public partial class Directorio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        private static string GetConnectionStringByName(string p)
        {
            string returnValue = null;
            System.Configuration.ConnectionStringSettings settings =
            System.Configuration.ConfigurationManager.ConnectionStrings[p];
            if (settings != null)
                returnValue = settings.ConnectionString;
            return returnValue;
        }

        [WebMethod]
        public static string CargarTablaNombre(string Nombre)
        {
            string tbl = "";

            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPQ_Portal_DirectorioNombre", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Nombre", Nombre);
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    tbl = "<thead><tr>" +
                        "<th>Nombre</th>" +
                        "<th>Puesto</th>" +
                        "<th>Secretaria</th>" +
                        "<th>Dependencia</th>" +
                        "<th>Correo</th>" +
                        "<th>Telefono</th>" +
                        "<th>Extension</th>" +
                        "<th>LugarTrabajo</th>" +
                        "</thead><tbody>";

                    while (dr.Read())
                    {
                        
                            tbl += "<tr><td>" + dr["Nombre"] + "</td>" +
                            "<td>" + dr["Puesto"] + "</td>" +
                            "<td>" + dr["Secretaria"] + "</td>" +
                            "<td>" + dr["Presupuestal"] + "</td>" +
                            "<td>" + dr["email"] + "</td>" +
                            "<td>" + dr["Telefono"] + "</td>" +
                            "<td>" + dr["Extension"] + "</td>" +
                            "<td>" + dr["LugarTrabajo"] + "</td>" +
                            "</tr>";
                       
                    }
                    dr.Close();
                    tbl += "</tbody>";
                }
                dr.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }
            cmd.Connection.Close();
            return tbl;
        }

        [WebMethod]
        public static string CargarTablaPuesto(string Puesto)
        {
            string tbl = "";

            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPQ_Portal_DirectorioPuesto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Puesto", Puesto);
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    tbl = "<thead><tr>" +
                        "<th>Nombre</th>" +
                        "<th>Puesto</th>" +
                        "<th>Secretaria</th>" +
                        "<th>Dependencia</th>" +
                        "<th>Correo</th>" +
                        "<th>Telefono</th>" +
                        "<th>Extension</th>" +
                        "<th>LugarTrabajo</th>" +
                        "</thead><tbody>";

                    while (dr.Read())
                    {

                        tbl += "<tr><td>" + dr["Nombre"] + "</td>" +
                        "<td>" + dr["Puesto"] + "</td>" +
                        "<td>" + dr["Secretaria"] + "</td>" +
                        "<td>" + dr["Presupuestal"] + "</td>" +
                        "<td>" + dr["email"] + "</td>" +
                        "<td>" + dr["Telefono"] + "</td>" +
                        "<td>" + dr["Extension"] + "</td>" +
                        "<td>" + dr["LugarTrabajo"] + "</td>" +
                        "</tr>";

                    }
                    dr.Close();
                    tbl += "</tbody>";
                }
                dr.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }
            cmd.Connection.Close();
            return tbl;
        }


        [WebMethod]
        public static string CargarTablaDependencia(string Dependencia)
        {
            string tbl = "";

            string conexion = GetConnectionStringByName("CS_RHEvolucion");
            SqlConnection con = new SqlConnection(conexion);
            SqlCommand cmd = new SqlCommand("SPQ_Portal_DirectorioDependencia", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Dependencia", Dependencia);
            try
            {
                cmd.Connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    tbl = "<thead><tr>" +
                        "<th>Nombre</th>" +
                        "<th>Puesto</th>" +
                        "<th>Secretaria</th>" +
                        "<th>Dependencia</th>" +
                        "<th>Correo</th>" +
                        "<th>Telefono</th>" +
                        "<th>Extension</th>" +
                        "<th>LugarTrabajo</th>" +
                        "</thead><tbody>";

                    while (dr.Read())
                    {

                        tbl += "<tr><td>" + dr["Nombre"] + "</td>" +
                        "<td>" + dr["Puesto"] + "</td>" +
                        "<td>" + dr["Secretaria"] + "</td>" +
                        "<td>" + dr["Presupuestal"] + "</td>" +
                        "<td>" + dr["email"] + "</td>" +
                        "<td>" + dr["Telefono"] + "</td>" +
                        "<td>" + dr["Extension"] + "</td>" +
                        "<td>" + dr["LugarTrabajo"] + "</td>" +
                        "</tr>";

                    }
                    dr.Close();
                    tbl += "</tbody>";
                }
                dr.Dispose();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally { if (cmd.Connection.State == ConnectionState.Open) { cmd.Connection.Close(); } }
            cmd.Connection.Close();
            return tbl;
        }




    }
}