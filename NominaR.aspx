﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NominaR.aspx.cs" Inherits="Emplea.NominaR" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset='utf-8'>
    <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="Scripts/img/favicon.ico" type="Scripts/image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="follow, index">
    <meta name="generator" content="Gobierno del Estado de Nuevo León">
    <title> Nómina | Gobierno del Estado de Nuevo León</title>
   
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/dropdowns/">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="Scripts/css/calendar.css">
    <style>
    @import url("Scripts/css/bootstrap-grid.css");
    @import url("Scripts/css/style.css");

    </style>
    <link href="Scripts/PNotify/pnotify.custom.css" media="all" rel="stylesheet" type="text/css" />
    <link href="Scripts/css/dropdowns.css" rel="stylesheet">
   <link href="Scripts/assets/dist/css/bootstrap.min.css" rel="stylesheet">
   

    <style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap');
    </style>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700;800;900&display=swap');
    </style>
    <style>::-webkit-scrollbar {
      width: 8px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
    }
    
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    } @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");</style>
    <style type="text/css">
        body {
            font-family: "Roboto Condensed", Helvetica, sans-serif;
            background-color: #f7f7f7;
        }
        .container { margin: 150px auto; max-width: 960px; }
        a {

            text-decoration: none;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        table, th, td {
           border: 1px solid #bbb;
           text-align: left;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        th {
            background-color: #ddd;
        }
        th,td {
            padding: 5px;
        }
        button {
            cursor: pointer;
        }
        /*Initial style sort*/
        .tablemanager th.sorterHeader {
            cursor: pointer;
        }
        .tablemanager th.sorterHeader:after {
            content: " \f0dc";
            font-family: "FontAwesome";
        }
        /*Style sort desc*/
        .tablemanager th.sortingDesc:after {
            content: " \f0dd";
            font-family: "FontAwesome";
        }
        /*Style sort asc*/
        .tablemanager th.sortingAsc:after {
            content: " \f0de";
            font-family: "FontAwesome";
        }
        /*Style disabled*/
        .tablemanager th.disableSort {

        }
        #for_numrows {
            padding: 10px;
            float: left;
        }
        #for_filter_by {
            padding: 10px;
            float: right;
        }
        #pagesControllers {
            display: block;
            text-align: center;
        }
    </style>
</head>
    <body className='snippet-body'>
    <body id="body-pd" class="page page-capital-humano nomina">
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search">
         <%-- <input type="search" class="form-control" placeholder="Buscar..." aria-label="Search">--%>
        </form>

        <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <div class="datos-user">                
                 <div class="name"> <p style="font-family: 'Century Gothic'" id="lblUsuario" runat="server"></p></div>
                <div class="dependencia"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblDependencia" runat="server"></p></div>
               
            </div>
            <img src="Scripts/img/userBlue.jpg" alt="mdo" width="32" height="32" class="rounded-circle">
            
          </a>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                <div class="num-empleado">
                    <div class="num-empleado-label">
                        <p style="font-family: 'Century Gothic'" id="lblUsuario2" runat="server"></p>
                    </div>
                    <div class="num-empleado-codigo">
                        <p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblDependencia2" runat="server"></p>
                    </div>
                </div>
                <div class="usuario-perfil-controles">
                    <%--  <button class="btn green">Activo</button>
                <button class="btn brown">Editar Perfil</button>--%>
                </div>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a class="dropdown-item" href="Perfil.aspx"><img src="Scripts/img/Mi_cuenta.svg">Mi Cuenta</a></li>
                <li><a class="dropdown-item" href="Login.aspx"><img src="Scripts/img/cerrar_sesion.svg">Cerrar Sesión</a></li>
            </ul>
        </div>
        </header>
        <div class="l-navbar" id="nav-bar">
            <nav class="nav">
                <div> 
                    <a href="Default.aspx" class="nav_logo"> <div class="logo_menu"></div></a>
                    <div class="nav_list"> 
                         <a href="Default.aspx" class="nav_link"> <img src="Scripts/img/inicio.svg"> <span class="nav_name">Inicio</span> </a> 
                        <a href="NominaR.aspx" class="nav_link active"> <img src="Scripts/img/LS_Nomina.svg"> <span class="nav_name">Nómina</span> </a>
                        <a href="https://sa.nl.gob.mx/Rhservicios" class="nav_link" target="_blank"> <img src="Scripts/img/A_Educacion.svg" > <span class="nav_name">Apoyo Escolar Coporaciones Policiacas</span> </a>
                        <a href="https://sa.nl.gob.mx/Rhservicios" class="nav_link" target="_blank"> <img src="Scripts/img/Ahorro_Retiro.svg"> <span class="nav_name">Fondo de ahorro para el retiro<div class="prox">Próximamente</div></span> </a> 
                        <a href="http://profesionalizacion.nl.gob.mx/index.html" target="_blank" class="nav_link"> <img src="Scripts/img/Cursos_Online.svg" > <span class="nav_name">Cursos en línea</span> </a>   
                        <a href="https://sa.nl.gob.mx/Rhservicios/Vacacion.aspx" class="nav_link" target="_blank"> <img src="Scripts/img/LS_Descuentos.svg" > <span class="nav_name">Vacaciones</span> </a> 
                        <a href="Patrimonial.aspx" class="nav_link"> <img src="Scripts/img/Informe_Declaracion_Patrimonial.svg"> <span class="nav_name">Informes de ingreso para declaración patrimonial</span> </a> 
                        <a href="Forma37.aspx" class="nav_link"> <img src="Scripts/img/LS_C_Retencion.svg"> <span class="nav_name">Constancias de sueldos y retenciones (Forma 37)</span> </a> 
                        <a href="Comparte.aspx" class="nav_link"> <img src="Scripts/img/Boletin.svg"> <span class="nav_name">Boletín ‘Comparte’ </span> </a> 
                        <a href="Perfil.aspx" class="nav_link"> <img src="Scripts/img/Mi_cuenta.svg"> <span class="nav_name">Mi Cuenta</span> </a> 
                       </div>
                    <div class="menu-mensaje">
                        <img src="Scripts/img/Lupa.png">
                        <h4>¿Necesitas ayuda?</h4>
                        <p>Marca al 123 desde tu teléfono de oficina o escríbenos</p>

                        <p><strong>cast@nuevoleon.gob.mx</strong></p>
                    </div>
                </div>
            </nav>
        </div>
    <!--Container Main start-->
    <div class="content-nomina">
        <h4>Nómina</h4>
        <div class="border-small"></div>
        <br>

        <div class="main-container container--wide container">
            <div class="container">
            <!-- Table start -->
                    <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Fecha de pago</th>
                  <th scope="col">Recibo simple</th>
                  <th scope="col">CFDI</th>
                  <th scope="col">Tipo de pago</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Ordinario</td>
                </tr>
                <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <img src="img/download-bottom.png"></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Extraordinario</td>
                </tr>
                 <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Ordinario</td>
                </tr>
                <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Extraordinario</td>
                </tr>
                 <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Ordinario</td>
                </tr>
                <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Extraordinario</td>
                </tr>
                 <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Ordinario</td>
                </tr>
                <tr>
                  <th scope="row">29 abril 2022</th>
                  <td><button class="btn">PDF <div class="icon-download"></div></button></td>
                  <td><button class="btn">PDF <div class="icon-download"></div></button><button class="btn">XML<div class="icon-download"></div></button></td>
                  <td>Extraordinario</td>
                </tr>
              </tbody>
            </table>
            </div>
            
           <div class="container ambiental-economico">
                <h1 class="title-seccion"><strong>Impacto ambiental y económico</strong> al eliminar  la impresión del recibo de nómina.</h1>
                <div class="row">
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto hojas"></div>
                            <p>Se evitan imprimir</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber" data-slno='1' data-min='1050000' data-max='1052784' data-delay='15' data-increment="15">0</div>
                            </div>
                            <p>Hojas papel bond</p>
                        </div>
                        <div class="text-impacto"><p>Anualmente, se evita la impresión de <strong>5.3 toneladas</strong> de papel bond requeridas para la emisión del recibo de nómina quincenal.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto arbol"></div>
                            <p>Se evitan talar</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber2" data-slno='1' data-min='0' data-max='95' data-delay='10' data-increment="9">0</div>
                            </div>
                            <p>Árboles</p>
                        </div>
                        <div class="text-impacto"><p>Indirectamente, se evita la tala anual de árboles requeridos para el proceso de producción de papel celulosa virgen.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto dinero"></div>
                            <p>Ahorros económicos</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber3" data-slno='1' data-min='400743' data-max='405743' data-delay='15' data-increment="15">0</div>
                            </div>
                            <p>Pesos anuales</p>
                        </div>
                        <div class="text-impacto"><p>Directamente, se generan ahorros económicos derivados de la adquisición de papel bond, tóners de tinta, así como en el gasto de gasolina.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto agua"></div>
                            <p>Se evita el uso de</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber4" data-slno='1' data-min='780000' data-max='789588' data-delay='15' data-increment="15">0</div>
                            </div>
                            <p>Litros de agua</p>
                        </div>
                        <div class="text-impacto"><p>Indirectamente, se evita el uso del agua requerida para el proceso de producción de papel celulosa virgen.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto auto"></div>
                            <p>Se evita el consumo</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento"id="masknumber5"  data-slno='1' data-min='0' data-max='262' data-delay='10' data-increment="9">0</div>
                            </div>
                            <p>Litros de gasolina</p>
                        </div>
                        <div class="text-impacto"><p>Directamente, se evita el consumo de gasolina para los traslados en la entrega de los recibos de nómina.</p></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto madera"></div>
                            <p>Se evita el uso de</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber6"  data-slno='1' data-min='0' data-max='12.6' data-delay='10' data-increment="9">12.6</div>
                            </div>
                            <p>Toneladas de madera</p>
                        </div>
                        <div class="text-impacto"><p>Indirectamente, se evita el uso de madera proveniente de la tala de árboles requeridos para el proceso de producción de papel.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto energia"></div>
                            <p>Se evita el gasto de</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber7" data-slno='1' data-min='30847' data-max='36847' data-delay='15' data-increment="15">0</div>
                            </div>
                            <p>KWh energía</p>
                        </div>
                        <div class="text-impacto"><p>Indirectamente, se evita el gasto en la energía eléctrica requerida para el proceso de producción de papel celulosa virgen.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto CO"></div>
                            <p>Se evitan emitir</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber8" data-slno='1' data-min='10000' data-max='17371' data-delay='10' data-increment="9">0</div>
                            </div>
                            <p>Kilogramos de CO2</p>
                        </div>
                        <div class="text-impacto"><p>Indirectamente, se evita la emisión de Dióxido de Carbono que resulta del consumo de energéticos (combustibles y electricidad) durante la manufactura del papel.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto tinta"></div>
                            <p>Se evita el consumo</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber9" data-slno='1' data-min='0' data-max='42' data-delay='10' data-increment="9">0</div>
                            </div>
                            <p>Toners de tinta</p>
                        </div>
                        <div class="text-impacto"><p>Directamente, se evita el consumo de tóners de tinta requeridos para la impresión de los recibos.</p></div>
                    </div>
                    <div class="col-impacto">
                        <div class="cuadro-azul">
                            <div class="img-impacto mano-obra"></div>
                            <p>Se evita mano de obra</p>
                            <div class="numeralia">
                                <div class="numscroller numscroller-big-bottom num-aumento" id="masknumber10" data-slno='1' data-min='0' data-max='7380' data-delay='10' data-increment="9">0</div>
                            </div>
                            <p>Horas hábiles</p>
                        </div>
                        <div class="text-impacto"><p>Directamente, se evita la asignación de mano de obra para la impresión, acomodo y entrega de los recibos de nómina. Dicha mano de obra será destinada a actividades que agreguen valor al proceso de recursos humanos.</p></div>
                    </div>
                </div>

        </div>
        
    </div>
    <!--Container Main end-->
    <footer class="blue">
        <div class="row">
            <div class="col-12 col-xl-2 col-md-12">
                <div class="region region-footer-logo">
                    <img class="footer_img" src="Scripts/img/logo-escudo-footer.svg" alt="nuevo-leon" title="Nuevo León">
                </div>
            </div>
            <div class="col-12 col-xl-5 col-md-8 margin-auto">
                <h3>Servicios para colaboradores</h3>
		      <div class="col-12 col-xl-6 col-md-6 izq">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="NominaR.aspx" title="">Nómina</a></li>
					<li class="leaf"><a  href="https://sa.nl.gob.mx/Rhservicios" target="_blank" title="">Apoyo Escolar SSP</a></li>
					<li class="leaf"><a href="https://www.isssteleon.gob.mx/afiliado/solicitud_pcp.html" target="_blank" title="ISSSTELEON">Préstamos</a></li>
					<%--<li class="leaf"><a href="#" title="">Prestaciones</a></li>--%>
					<li class="leaf"><a href="https://sa.nl.gob.mx/Rhservicios" target="_blank" title="RHSERVICIOS">Vacaciones </a></li>
					<%--<li class="last"><a href="#" title="">Refrendo</a></li>--%>
				</ul>
		      </div>
		      <div class="col-12 col-xl-6 col-md-6 der">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="Forma37.aspx" title="">Sueldos y retenciones</a></li>
					<li class="leaf"><a href="Patrimonial.aspx" title="">Declaración patrimonial</a></li>
					<%--<li class="leaf"><a href="#" title="">Constancia laboral</a></li>--%>
					<li class="leaf"><a href="http://profesionalizacion.nl.gob.mx/index.html" target="_blank" title="">Capacitación</a></li>
					<%--<li class="leaf"><a href="#" title="">Un servicio más</a></li>
					<li class="last"><a href="#" title="">Otro más</a></li>--%>
				</ul>
		      </div>
            </div>
            <div class="col-12 col-xl-2 col-md-8 margin-auto">
               <%-- <h3>Atención y soporte</h3>--%>
                <div class="col-12 col-xl-12 der">
                  <%--  <p>¿Tienes alguna solicitud de mantenimiento o requieres apoyo tecnológico?</p>
                    <p>Contáctanos y estaremos contigo para ayudarte</p>--%>
                </div>
            </div>
            <div class="col-12 col-xl-3 col-md-8 margin-auto">
                <h3></h3>
                <div class="col-12 col-xl-12">
                    <img class="footer_img" src="Scripts/img/logo_administracion.png" alt="administracion" title="Administración">
                </div>
            </div>
            <div class="social der">
                <div class="der">
                    <div class="facebook alineado"><a href="https://www.facebook.com/gobiernonuevoleon/"><img src="Scripts/img/Facebook.svg"></a></div>
		    		<div class="instagram alineado"><a href="https://www.instagram.com/nuevoleonmx"><img src="Scripts/img/Instagram.svg"></a></div>
		    		<div class="youtube alineado"><a href="https://www.youtube.com/user/GobiernoNuevoLeon"><img src="Scripts/img/Youtube.svg"></a></div>
		    
                </div>
            </div>
            <div class="col-12 center">
                <h5>Gobierno del Estado de Nuevo León</h5>
            </div>
        </div>
    </footer>
    <footer class="white">
        <div class="alineado text-dorado">SECRETARÍA DE ADMINISTRACIÓN</div>
        <div class="alineado"><img src="Scripts/img/footer_leon.svg"></div>
        <div class="alineado text-dorado">UN NUEVO NUEVO LEÓN</div>
    </footer>
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript' src='#'></script>
    <script type='text/javascript'>document.addEventListener("DOMContentLoaded", function(event) {
   
        const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)

        // Validate that all variables exist
        if(toggle && nav && bodypd && headerpd){
        toggle.addEventListener('click', ()=>{
        // show navbar
        nav.classList.toggle('show')
        // change icon
        toggle.classList.toggle('bx-x')
        // add padding to body
        bodypd.classList.toggle('body-pd')
        // add padding to header
        headerpd.classList.toggle('body-pd')
        })
        }
        }

        showNavbar('header-toggle','nav-bar','body-pd','header')

        /*===== LINK ACTIVE =====*/
        const linkColor = document.querySelectorAll('.nav_link')

        function colorLink(){
        if(linkColor){
        linkColor.forEach(l=> l.classList.remove('active'))
        this.classList.add('active')
        }
        }
        linkColor.forEach(l=> l.addEventListener('click', colorLink))

         // Your code to run since DOM is loaded and ready
        });
    </script>
    <script type='text/javascript'>var myLink = document.querySelector('a[href="#"]');
    myLink.addEventListener('click', function(e) {
      e.preventDefault();
    });</script>
                  
   <script src="Scripts/assets/dist/js/bootstrap.bundle.min.js"></script>
   <script src="Scripts/js/jquery.min.js"></script>
    <script src="Scripts/js/popper.js"></script>
    <script src="Scripts/js/bootstrap.min.js"></script>
    <script src="Scripts/js/main.js"></script>
    <script type="text/javascript" src="Scripts/js/tableManager.js"></script>
    <script src="Scripts/js/numscroller-1.0.js"></script>   
        <script type="text/javascript">
            $('#masknumber').text(function () {
                var str = $(this).html() + '';
                x = str.split('.');
                x1 = x[0]; x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                $(this).html(x1 + x2);
            });
            $('#masknumber2').text(function () {
                var str = $(this).html() + '';
                x = str.split('.');
                x1 = x[0]; x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                $(this).html(x1 + x2);
            });
            $('#masknumber3').text(function () {
                var str = $(this).html() + '';
                x = str.split('.');
                x1 = x[0]; x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                $(this).html(x1 + x2);
            });


        </script>











  
    <script>
try {
  fetch(new Request("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", { method: 'HEAD', mode: 'no-cors' })).then(function(response) {
    return true;
  }).catch(function(e) {
    var carbonScript = document.createElement("script");
    carbonScript.src = "//cdn.carbonads.com/carbon.js?serve=CK7DKKQU&placement=wwwjqueryscriptnet";
    carbonScript.id = "_carbonads_js";
    document.getElementById("carbon-block").appendChild(carbonScript);
  });
} catch (error) {
  console.log(error);
}
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>  
         <script type="text/javascript" src="Scripts/PNotify/pnotify.custom.js"></script>
        <script type="text/javascript" src="ScriptsAspx/NominaR.js"></script>


    </body>
</html>