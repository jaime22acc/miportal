﻿Inicio();

function Inicio() {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Directorio.aspx/CargarTablaNombre",
        dataType: "json",
        data: JSON.stringify({ Nombre: 'VALERIA NAS' }),
        success: function (data) {
            $("#Tabla").html(data.d);

            Tabla = $("#Tabla").tablemanager({
                firstSort: [[1, 0], [2, 0], [3, 0], [4, 0]],
                disable: ["last"],
                appendFilterby: true,
                dateFormat: [[1, "dd-mm-yyyy"]],
                debug: true,
                vocabulary: {
                    voc_filter_by: 'Filtrar',
                    voc_type_here_filter: 'Filtro...',
                    voc_show_rows: 'Reglones'
                },
                pagination: true,
                showrows: [5, 10, 20, 50, 100],
                disableFilterBy: [5]

            });

        },

        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }
    });


}





$("#btnNombre").click(function () {

  
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "Directorio.aspx/CargarTablaNombre",
            dataType: "json",
            data: JSON.stringify({ Nombre: $("#txtNombre").val() }),
            success: function (data) {
                $("#Tabla").html(data.d);

                Tabla = $("#Tabla").tablemanager({
                    firstSort: [[1, 0], [2, 0], [3, 0], [4, 0]],
                    disable: ["last"],
                    appendFilterby: true,
                    dateFormat: [[1, "dd-mm-yyyy"]],
                    debug: true,
                    vocabulary: {
                        voc_filter_by: 'Filtrar',
                        voc_type_here_filter: 'Filtro...',
                        voc_show_rows: 'Reglones'
                    },
                    pagination: true,
                    showrows: [5, 10, 20, 50, 100],
                    disableFilterBy: [5]

                });
                
            },

        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }
    });


});




$("#btnPuesto").click(function () {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Directorio.aspx/CargarTablaPuesto",
        dataType: "json",
        data: JSON.stringify({ Puesto: $("#txtPuesto").val() }),
        success: function (data) {
            $("#Tabla").html(data.d);

            Tabla = $("#Tabla").tablemanager({
                firstSort: [[1, 0], [2, 0], [3, 0], [4, 0]],
                disable: ["last"],
                appendFilterby: true,
                dateFormat: [[1, "dd-mm-yyyy"]],
                debug: true,
                vocabulary: {
                    voc_filter_by: 'Filtrar',
                    voc_type_here_filter: 'Filtro...',
                    voc_show_rows: 'Reglones'
                },
                pagination: true,
                showrows: [5, 10, 20, 50, 100],
                disableFilterBy: [5]

            });

        },

        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }
    });



});


$("#btnDependencia").click(function () {

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Directorio.aspx/CargarTablaDependencia",
        dataType: "json",
        data: JSON.stringify({ Dependencia: $("#txtDependencia").val() }),
        success: function (data) {
            $("#Tabla").html(data.d);

            Tabla = $("#Tabla").tablemanager({
                firstSort: [[1, 0], [2, 0], [3, 0], [4, 0]],
                disable: ["last"],
                appendFilterby: true,
                dateFormat: [[1, "dd-mm-yyyy"]],
                debug: true,
                vocabulary: {
                    voc_filter_by: 'Filtrar',
                    voc_type_here_filter: 'Filtro...',
                    voc_show_rows: 'Reglones'
                },
                pagination: true,
                showrows: [5, 10, 20, 50, 100],
                disableFilterBy: [5]

            });

        },

        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }
    });


});