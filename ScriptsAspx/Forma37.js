﻿GetTabla();



function GetTabla() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Forma37.aspx/ObtenerTabla",
        dataType: "json",
        data: JSON.stringify({}),
        success: function (data) {
            $("#Tabla").html(data.d);

            Tabla = $("#Tabla").tablemanager({
                firstSort: [[3, 'desc'], [1, 0], [2, 0]],
                disable: ["last"],
                appendFilterby: true,
                dateFormat: [[1, "dd-mm-yyyy"]],
                debug: true,
                vocabulary: {
                    voc_filter_by: 'Filtrar',
                    voc_type_here_filter: 'Filtro...',
                    voc_show_rows: 'Constancias por pestaña'
                },
                pagination: true,
                showrows: [5, 10, 20, 50, 100],
                disableFilterBy: [1,2, 4, 5, 6, 7]

            });         
            $('#Tabla').on('click', '.btnPDF', function () {
                GenerarForma($(this).attr("RFC"), $(this).attr("Anio"));
            });
        },



        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}


function GenerarForma(RFC, Anio) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Forma37.aspx/GeneraForma",
        dataType: "json",
        data: JSON.stringify({ RFC: RFC, Anio: Anio }),

        success: function (data) {

            var pdf_link = data.d.Ruta;

            window.open(pdf_link, '_blank');

        },


        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}
