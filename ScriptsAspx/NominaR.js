﻿GetTabla();



function GetTabla() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "NominaR.aspx/ObtenerTabla",
        dataType: "json",
        data: JSON.stringify({}),
        success: function (data) {
            $("#Tabla").html(data.d);
  
            Tabla = $("#Tabla").tablemanager({
                firstSort: [[1, 'desc'],[3, 0], [2, 0] ],
                disable: ["last"],
                appendFilterby: true,
                dateFormat: [[1, "dd-mm-yyyy"]],
                debug: true,
                vocabulary: {
                    voc_filter_by: 'Filtrar',
                    voc_type_here_filter: 'Filtro...',
                    voc_show_rows: 'Recibos por pestaña'
                },
                pagination: true,
                showrows: [5, 10, 20, 50, 100],
                disableFilterBy: [2,5,6,7]
                
            });
            $('#Tabla').on('click', '.btnRec', function () {
                GenerarRecibo($(this).attr("IdEmpleado"), $(this).attr("Fecha"));
            });
            $('#Tabla').on('click', '.btnXML', function () {
                GenerarXML($(this).attr("UUID"));
            });
            $('#Tabla').on('click', '.btnPDF', function () {
                GenerarPDFCFDI($(this).attr("UUID"));
            });
        },



        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}



function GenerarRecibo(IdEmpleado, Fecha) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "NominaR.aspx/GeneraReciboSimple",
        dataType: "json",
        data: JSON.stringify({ IdEmpleado: IdEmpleado, Fecha: Fecha }),

        success: function (data) {

            var pdf_link = data.d.Ruta;

            window.open(pdf_link, '_blank');
          
        },


        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}


function GenerarPDFCFDI(UUID) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "NominaR.aspx/GeneraCFDIPDF",
        dataType: "json",
        data: JSON.stringify({ UUID: UUID }),

        success: function (data) {

            var pdf_link = data.d.Ruta;

            window.open(pdf_link, '_blank');
            
        },


        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}



function GenerarXML(UUID) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "NominaR.aspx/GeneraCFDIXML",
        dataType: "json",
        data: JSON.stringify({ UUID: UUID }),

        success: function (data) {

            var pdf_link = data.d.Ruta;

            window.open(pdf_link, '_blank');

        },


        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}



