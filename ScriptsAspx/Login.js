﻿
Inicio();

function Inicio() {

    new PNotify({
        title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
        text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658">Si no conoces o no recuerdas tu usuario intenta acceder con tu <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658">número de empleado, RFC, CURP o tu correo antes de la arroba,</span> es decir, <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658">nombre.apellido</span>@nuevoleon.gob.mx. Si no recuerdas tu contraseña llama a los teléfonos 81 2020 1152, 1154, 6794, 6795 <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> o a la extensión 123</span> desde la red telefónica interna. </span>',
        type: "info",
        delay: 5000,
        timer: 5000,
        shadow: true,
        animate: true,
        stack: false,
        hide: true,
        sticker: false,
        width: '25%',
        styling: "fontawesome",
        addClass: 'stack-bottomright error',
        animate: {

            inClass: 'bounceIn',
            outClass: 'bounceOut'
        },
        nonblock: {
            nonblock: true
        }


    });



}


$("#btnIngresar").click(function () {


    
        location.href = 'Default.aspx';
 

});

$("#txtContrasena").keypress(function (e) {
    if (e.which === 13 && $("#txtContrasena").val() !== '') {

        /*fc_login($("#txtUsuario").val(), $("#txtContrasena").val());*/
        location.href = 'Default.aspx';
    }
});



$("#txtUsuario").keypress(function (e) {
    if (e.which === 13 && $("#txtUsuario").val() !== '') {

        $("#txtContrasena").focus();
    }
    else if (e.which === 13 && $("#txtUsuario").val() === '') {

        new PNotify({
            title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
            text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658"> <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> </span> ingrese una contraseña </span>',
            type: "error",
            delay: 5000,
            timer: 5000,
            shadow: true,
            animate: true,
            stack: false,
            hide: true,
            sticker: false,
            width: '25%',
            styling: "fontawesome",
            addClass: 'stack-bottomright error',
            animate: {

                inClass: 'bounceIn',
                outClass: 'bounceOut'
            },
            nonblock: {
                nonblock: true
            }


        });

    }

});


function myFunction2() {
    var x = document.getElementById("txtContrasena");
    var y = document.getElementById("cbox2");
    if (x.type === "text") {
        x.type = "password";
    }

    $("#cbox2").prop('checked', false);
    $("#txtContrasena").val("");


}

function fc_login(UserName, Password) {


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "Login.aspx/Logines",
        data: JSON.stringify({
            UserName: UserName,
            Password: Password
        }),

        dataType: "json",
        success: function (data) {

            var arr = new Array();
            console.log(data);

            if (data.d == "OK") {

                new PNotify({
                    title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
                    text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658"> <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> </span>' + 'Bienvenido Usuario : ' + UserName + '</span>',
                    type: "success",
                    delay: 5000,
                    timer: 5000,
                    shadow: true,
                    animate: true,
                    stack: false,
                    hide: true,
                    sticker: false,
                    width: '25%',
                    styling: "fontawesome",
                    addClass: 'stack-bottomright error',
                    animate: {

                        inClass: 'bounceIn',
                        outClass: 'bounceOut'
                    },
                    nonblock: {
                        nonblock: true
                    }
                });




                myFunction2();
                window.location.assign("Default.aspx");
            }
            else {
                new PNotify({
                    title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
                    text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658"> <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> </span>' + data.d + '</span>',
                    type: "info",
                    delay: 5000,
                    timer: 5000,
                    shadow: true,
                    animate: true,
                    stack: false,
                    hide: true,
                    sticker: false,
                    width: '25%',
                    styling: "fontawesome",
                    addClass: 'stack-bottomright error',
                    animate: {

                        inClass: 'bounceIn',
                        outClass: 'bounceOut'
                    },
                    nonblock: {
                        nonblock: true
                    }


                });

            }

        },
        error: function (result) {

            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
                text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658"> <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> </span>' + "Error en el proceso de autentificación jq" + '</span>',
                type: "error",
                delay: 5000,
                timer: 5000,
                shadow: true,
                animate: true,
                stack: false,
                hide: true,
                sticker: false,
                width: '20%',
                styling: "fontawesome",
                addClass: 'stack-bottomright error',
                animate: {

                    inClass: 'bounceIn',
                    outClass: 'bounceOut'
                },
                nonblock: {
                    nonblock: true
                }


            });



        }

    });

}
