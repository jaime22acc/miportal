﻿CargarDatosInicio();



function CargarDatosInicio() {
     $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Perfil.aspx/getDatosEmpleado",
        dataType: "json",
        data: '{}',
        success: function (data) {
            $('html,body').animate({ scrollTop: $("body").offset().top }, 'fast');

            $('#txtEmail').val(data.d.Correo);
            $('#txtTelefono').val(data.d.Telefono);
            $('#txtExtension').val(data.d.Extension);

            
            document.getElementById("lblNoEmpleado").innerHTML = data.d.NoEmpleado;  
            document.getElementById("lblNombre").innerHTML = data.d.Nombre;
            document.getElementById("lblSecretaria").innerHTML = data.d.Secretaria;
            document.getElementById("lblPresupuestal").innerHTML = data.d.Dependencia;           
            document.getElementById("lblContrato").innerHTML = data.d.TipoEmpleado;
            document.getElementById("lblFechaIngreso").innerHTML = data.d.FechaIngreso;
            document.getElementById("lblDispositivo").innerHTML = data.d.Dispositivo;
            document.getElementById("lblLugarTrabajo").innerHTML = data.d.LugarTrabajo;

   
            
        },


         error: function (xhr, status, error) {
             var err = eval("(" + xhr.responseText + ")");
             var errorMessage = xhr.status + ': ' + xhr.statusText
             new PNotify({
                 title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                 text: err.Message + ' ' + status + ' ' + errorMessage,
                 type: 'info',
                 icon: 'fa fa-check-square-o fa-2x',
                 delay: 5000,
                 timer: 10000,
                 shadow: true,
                 animate: {
                     enter: 'animated fadeInDown',
                     exit: 'animated fadeOutUp'
                 }
             });

         }
     });

}



$("#btnActualizar").click(function () {

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Perfil.aspx/Actualizar",
        dataType: "json",
        data: JSON.stringify({ IdEmpleado: document.getElementById("lblNoEmpleado").innerHTML, Email: $("#txtEmail").val(), Telefono: $("#txtTelefono").val(), Extension: $("#txtExtension").val() }),
        success: function (data) {
            $('html,body').animate({ scrollTop: $("body").offset().top }, 'fast');

            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold; font-family:Century Gothic; color:#c18658"> </span>',
                text: '<span style="font-size: 1.0em; font-family:Century Gothic; font-color:#c18658"> <span style="font-size: 1.0em; font-weight: bold; font-family: Century Gothic; font-color:#c18658"> </span>' + 'Se actualizó su información exitosamente  </span>',
                type: "success",
                delay: 5000,
                timer: 5000,
                shadow: true,
                animate: true,
                stack: false,
                hide: true,
                sticker: false,
                width: '25%',
                styling: "fontawesome",
                addClass: 'stack-bottomright error',
                animate: {

                    inClass: 'bounceIn',
                    outClass: 'bounceOut'
                },
                nonblock: {
                    nonblock: true
                }
            });



        },


        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }
    });


});
