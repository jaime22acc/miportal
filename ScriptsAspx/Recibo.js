﻿GetTabla();



function GetTabla() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Recibo.aspx/ObtenerTabla",
        dataType: "json",        
        data: JSON.stringify({}),
        success: function (data) {
            $("#Tabla").html(data.d);
            $("#Tabla").dataTable().fnDestroy();
            Tabla = $("#Tabla").DataTable({
                dom: 'Blfrtip',
                buttons: ['copy', 'csv', 'excel'],
                fixedHeader: false,
                "language": { "url": "Scripts/Spanish.json" },
                "bDestroy": true,
                "paging": true,
                "ordering": false,
            });
            $('#Tabla').unbind('click');
            $('#Tabla').on('click', '.btnXml', function () {
                var Liga = $(this).attr("Archivo");  
                window.open(Liga, "Anexo", "height=600,width=600");
            });
            $('#Tabla').on('click', '.btnRec', function () {               
                GenerarRecibo($(this).attr("IdEmpleado"), $(this).attr("Fecha"));
            });

          },
      

     
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' +status + ' '+ errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}



function GenerarRecibo(IdEmpleado, Fecha) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "Recibo.aspx/GeneraPDF",
        dataType: "json",
        data: JSON.stringify({ IdEmpleado: IdEmpleado, Fecha : Fecha }),

        success: function (data) {

            var pdf_link = data.d.Ruta;

            changeSrcEmbed2(pdf_link);
            window.open(pdf_link, "Recibo", "height=600,width=600");

        },
       

            error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            var errorMessage = xhr.status + ': ' + xhr.statusText
            new PNotify({
                title: '<span style="font-size: 1.5em; font-weight: bold">¡¡Advertencia!!</span>',
                text: err.Message + ' ' + status + ' ' + errorMessage,
                type: 'info',
                icon: 'fa fa-check-square-o fa-2x',
                delay: 5000,
                timer: 10000,
                shadow: true,
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                }
            });

        }



    });
}


function changeSrcEmbed2(src) {  
    var element = document.getElementById('Mymodalx');
    var id = element.id;
    element.src = src;
    var embedOld = document.getElementById(id);
    var parent = embedOld.parentElement;
    var newElement = element;
    document.getElementById(id).remove();
    parent.append(newElement);
    $("#myModal").modal("show");
}


