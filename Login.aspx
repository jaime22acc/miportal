﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Emplea.Login" %>

<html>
<head>
    <meta charset="UTF-8">
    <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="Scripts/img/favicon.ico" type="image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="follow, index">
    <meta name="generator" content="Gobierno del Estado de Nuevo León"> 
    <title> Login | Gobierno del Estado de Nuevo León</title>
 	<link href="Scripts/css/bootstrap.min.css" rel="stylesheet">

    <style>
    @import url("Scripts/css/bootstrap-grid.css");
    @import url("Scripts/css/style.css");

    </style>
    <link href="Scripts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap');
    </style>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700;800;900&display=swap');
    </style>
	<link href="Scripts/PNotify/pnotify.custom.css" media="all" rel="stylesheet" type="text/css" />
</head>

<body class="html page-login user-login-login">
	<header id="navbar" class="navbar navbar-fixed-top navbar-default">
    	<div class="container-fluid primary-color">
        	<div class="navbar-header">
                <a class="logo navbar-btn pull-left" href="Ingresa.aspx" title="Inicio">
                	<img src="Scripts/img/logo-header.svg" height="40" alt="Inicio">
                </a>
            </div>
	        <div class="label-header">
				<div class="label">Este es un sitio web oficial del Gobierno del Estado de Nuevo León.</div>
              <%--	<div class="label">Este es un sitio web oficial del Gobierno del Estado de Nuevo León. <a href="#">Aprende a identificarlos</a></div>--%>
	        </div>
        </div>
	</header>
	
	<main>
		<div class="col-12 col-md-12 col-lg-6 col-xl-6 login-user-izq img-login-user">
			<div class="corchete-blanco">
				<form class="form-signin">
                    <div class="d-flex mb-4">
						<img src="Scripts/img/logo_nl.svg" class="logo-login">
                        <div class="separator"></div>
						<img src="Scripts/img/Mi-Nuevo-Portal-_-Logo.png" class="logo-nuevo-portal">
                    </div>

				    <h2 class="mb-3 fw-normal font-primary ">Inicia sesión</h2>
				    <p class="text-center">Consulta tu recibo de nómina, prestaciones y beneficios en un solo lugar.</p>
				    <%--<p>Ingresa tu nombre de usuario y contraseña</p>--%>
				    <div class="form-floating">
                      <label class="font-primary label-input">Nombre de usuario</label>
				      <input type="text" class="form-control" id="txtUsuario" placeholder="Usuario">
				    </div>
				    <br>
				    <div class="form-floating">
                      <label class="font-primary label-input">Contraseña</label>
				      <input type="password" class="form-control" id="txtContrasena" placeholder="Contraseña">
				    </div>

							<button type="button" class="btn btn-info" id="btnIngresar" >
						<i style="color:White;"  aria-hidden='true' OnClick="Button1_Click"></i> Inicia sesión</button>  
					  <br>
				    <a href="recuperar-contraseña.html" class="recuperar-contrasena">¿Has olvidado tu contraseña?</a>
			  	</form>
		  	</div>
		</div>
		<%--<div class="col-12 col-md-12 col-lg-6 col-xl-6 login-user-der">
		  	<div class="img-corchete">
			</div>
	  	</div>--%>
	</main>

	<footer class="blue">
		<div class="row">
		    <div class="col-12 col-xl-2 col-md-12">
		      	<div class="region region-footer-logo">
				  	<img class="footer_img" src="Scripts/img/logo-escudo-footer.svg" alt="nuevo-leon" title="Nuevo León">
				</div>
		    </div>
		    <div class="col-12 col-xl-5 col-md-8 margin-auto">
		   <%--   <h3>Servicios para colaboradores</h3>--%>
		    <%--  <div class="col-12 col-xl-6 col-md-6 izq">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="#" title="">Nómina</a></li>
					<li class="leaf"><a href="#" title="">Fondo de ahorro</a></li>
					<li class="leaf"><a href="#" title="">Préstamos</a></li>
					<li class="leaf"><a href="#" title="">Prestaciones</a></li>
					<li class="leaf"><a href="#" title="">Vacaciones </a></li>
					<li class="last"><a href="#" title="">Refrendo</a></li>
				</ul>
		      </div>
		      <div class="col-12 col-xl-6 col-md-6 der">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="#" title="">Sueldos y retenciones</a></li>
					<li class="leaf"><a href="#" title="">Declaración patrimonial</a></li>
					<li class="leaf"><a href="#" title="">Constancia laboral</a></li>
					<li class="leaf"><a href="#" title="">Capacitaciones</a></li>
					<li class="leaf"><a href="#" title="">Un servicio más</a></li>
					<li class="last"><a href="#" title="">Otro más</a></li>
				</ul>
		      </div>--%>
		    </div>
		    <div class="col-12 col-xl-2 col-md-8 margin-auto">
		      	<%--<h3>Atención y soporte</h3>--%>
		      	<div class="col-12 col-xl-12 der">
			      	<%--<p>¿Tienes alguna solicitud de mantenimiento o requieres apoyo tecnológico?</p>
			      	<p>Contáctanos y estaremos contigo para ayudarte</p>--%>
		      	</div>
		    </div>
		    <div class="col-12 col-xl-3 col-md-8 margin-auto">
		    	<h3></h3>
		    	<div class="col-12 col-xl-12">
		      		<img class="footer_img" src="Scripts/img/logo_administracion.png" alt="administracion" title="Administración">
		      	</div>
		    </div>
		    <div class="social der">
		    	<div class="der">
		    		<div class="facebook alineado"><a href="https://www.facebook.com/gobiernonuevoleon/"><img src="Scripts/img/Facebook.svg"></a></div>
		    		<div class="instagram alineado"><a href="https://www.instagram.com/nuevoleonmx"><img src="Scripts/img/Instagram.svg"></a></div>
		    		<div class="youtube alineado"><a href="https://www.youtube.com/user/GobiernoNuevoLeon"><img src="Scripts/img/Youtube.svg"></a></div>
		    	</div>
		    </div>
		    <div class="col-12 center">
		    	<h5>Gobierno del Estado de Nuevo León</h5>
		    </div>
		</div>
	</footer>
	<footer class="white">
		<div class="alineado text-dorado">SECRETARÍA DE ADMINISTRACIÓN</div>
		<div class="alineado"><img src="Scripts/img/footer_leon.svg"></div>
		<div class="alineado text-dorado">UN NUEVO NUEVO LEÓN</div>
	</footer>
</body>
		<script src="Scripts/jquery-3.4.1.min.js"></script>
			<script src="Scripts/jquery-3.4.1.js"></script>
			
			<script type="text/javascript" src="Scripts/PNotify/pnotify.custom.js"></script>

	





	<script type="text/javascript" src="ScriptsAspx/Login.js"></script>
</html>