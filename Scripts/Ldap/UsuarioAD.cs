﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.DirectoryServices.ActiveDirectory;
using ActiveDs;

namespace Emplea
{
    public class UsuarioAD
    {
         public string Stowrite = "";
        private string SAMACOUNT = "";
        public string samacaunt { get { return SAMACOUNT; } set { SAMACOUNT = value; } }

        private string NOEMPLEADO = "";
        public string NoEmpleado { get { return NOEMPLEADO; } set { NOEMPLEADO = value; } }

        private string NOMBREEMPLEADO = "";
        public string NombreEmpleado { get { return NOMBREEMPLEADO; } set { NOMBREEMPLEADO = value; } }

        private string APELLIDOSEMPLEADO = "";
        public string ApellidosEmpleado { get { return APELLIDOSEMPLEADO; } set { APELLIDOSEMPLEADO = value; } }

        private string CATEGORIA = "";
        public string Categoria { get { return CATEGORIA; } set { CATEGORIA = value; } }

        private string RFC = "";
        public string Rfc
        {
            get { return RFC; }
            set
            {
                RFC = value;
                //if (value != "")
                //{
                //    if (!Regex.IsMatch(value, @"^([A-Z]{4})\d{6}([A-Z\w]{3})$")) { Stowrite = "Por favor ingrese un RFC valido."; }
                //    else { RFC = value; }
                //}
            }
        }

        private string CURP = "";
        public string Curp
        {
            get { return CURP; }
            set
            {
                CURP = value;
                //if (value != "")
                //{
                //    if (!Regex.IsMatch(value, "^[a-zA-Z]{4}\\d{6}[a-zA-Z]{6}\\d{2}$")) { Stowrite = "Por favor ingrese un CURP valido."; }
                //    else { CURP = value; }
                //}

            }
        }

        private string PUESTO = "";
        public string Puesto { get { return PUESTO; } set { PUESTO = value; } }

        private string SECRETARIA = "";
        public string Secretaria { get { return SECRETARIA; } set { SECRETARIA = value; } }

        private string DEPENDENCIA = "";
        public string Dependencia { get { return DEPENDENCIA; } set { DEPENDENCIA = value; } }

        private string SUBSECRETARIA = "";
        public string SubSecretaria { get { return SUBSECRETARIA; } set { SUBSECRETARIA = value; } }

        private string CALLEYNUMERO = "";
        public string CalleYNumero { get { return CALLEYNUMERO; } set { CALLEYNUMERO = value; } }

        private string COLONIA = "";
        public string Colonia { get { return COLONIA; } set { COLONIA = value; } }

        private string CODIGOPOSTAL = "";
        public string CodigoPostal
        {
            get { return CODIGOPOSTAL; }
            set
            {
                CODIGOPOSTAL = value;
                //if (value != "")
                //{
                //    if (!Regex.IsMatch(value, "^\\d{5}$")) { Stowrite = "Por favor ingrese un codigo postal valido."; }
                //    else { CODIGOPOSTAL = value; }
                //}

            }
        }

        private string CIUDAD = "";
        public string Ciudad { get { return CIUDAD; } set { CIUDAD = value; } }

        private string ESTADO = "";
        public string Estado { get { return ESTADO; } set { ESTADO = value; } }

        private string PAIS = "";
        public string Pais { get { return PAIS; } set { PAIS = value; } }

        private string OFICINA = "";
        public string oficina { get { return OFICINA; } set { OFICINA = value; } }

        private string EDIFICIO = "";
        public string Edificio { get { return EDIFICIO; } set { EDIFICIO = value; } }

        private string PISO = "";
        public string Piso { get { return PISO; } set { PISO = value; } }

        private string TELEFONOOFICINA = "";
        public string TelefonoOficina { get { return TELEFONOOFICINA; } set { TELEFONOOFICINA = value; } }

        private string TELEFONOMOVIL = "";
        public string TelefonoMovil { get { return TELEFONOMOVIL; } set { TELEFONOMOVIL = value; } }

        private string TELEFONOPARTICULAR = "";
        public string TelefonoParticular { get { return TELEFONOPARTICULAR; } set { TELEFONOPARTICULAR = value; } }

        private string JEFEINMEDIATO = "";
        public string JefeInmediato { get { return JEFEINMEDIATO; } set { JEFEINMEDIATO = value; } }

        private string OU = "";
        public string ou { get { return OU; } set { OU = value; } }

        private string PUBLISHEDAT = "";
        public string PublishedAt { get { return PUBLISHEDAT; } set { PUBLISHEDAT = value; } }

        private string MAILBOXSTORE = "";
        public string MailBoxStore { get { return MAILBOXSTORE; } set { MAILBOXSTORE = value; } }

        private DateTime LASTLOGON = DateTime.Today;
        public DateTime LastLogon { get { return LASTLOGON; } set { LASTLOGON = DateTime.Parse("1601-01-01 0:00"); } }

        private string FAC = "";
        public string Fac { get { return FAC; } set { FAC = value; } }

        private string INFO = "";
        public string info { get { return INFO; } set { INFO = value; } }


        private string IPPHONE = "";
        public string ipPhone { get { return IPPHONE; } set { IPPHONE = value; } }

        private string PAGER = "";
        public string pager { get { return PAGER; } set { PAGER = value; } }

        private string WEBPAGE = "";
        public string wWWHomePage { get { return WEBPAGE; } set { WEBPAGE = value; } }

        private string MEMBEROF = "";
        public string memberOf { get { return MEMBEROF; } set { MEMBEROF = value; } }

        private string  FECHAALTA  = "";
        public string   fechaAlta { get { return FECHAALTA; } set { FECHAALTA = value; } }

        private string FECHABAJA = "";
        public string fechaBaja { get { return FECHABAJA; } set { FECHABAJA = value; } }

        public bool CargarInfo(string user, HtmlTextWriter writer)
        {
            samacaunt = user;
            bool Encontrado = false;
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + user + ") )";
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);
            try
            {
                foreach (SearchResult resEnt in deSearch.FindAll())
                {
                    DirectoryEntry dentry = resEnt.GetDirectoryEntry();
                    if (dentry.Properties["employeeID"].Value != null) { NOEMPLEADO = dentry.Properties["employeeID"].Value.ToString(); } else { NOEMPLEADO = ""; }
                    if (dentry.Properties["givenName"].Value != null) { NOMBREEMPLEADO = dentry.Properties["givenName"].Value.ToString(); } else { NOMBREEMPLEADO = ""; }
                    if (dentry.Properties["sn"].Value != null) { APELLIDOSEMPLEADO = dentry.Properties["sn"].Value.ToString(); } else { APELLIDOSEMPLEADO = ""; }
                    if (dentry.Properties["nivelcategoria"].Value != null) { CATEGORIA = dentry.Properties["nivelcategoria"].Value.ToString(); } else { CATEGORIA = ""; }
                    if (dentry.Properties["rfc"].Value != null) { RFC = dentry.Properties["rfc"].Value.ToString(); } else { RFC = ""; }
                    if (dentry.Properties["curp"].Value != null) { CURP = dentry.Properties["curp"].Value.ToString(); } else { CURP = ""; }
                    if (dentry.Properties["title"].Value != null) { PUESTO = dentry.Properties["title"].Value.ToString(); } else { PUESTO = ""; }
                    if (dentry.Properties["Company"].Value != null) { SECRETARIA = dentry.Properties["Company"].Value.ToString(); } else { SECRETARIA = ""; }
                    if (dentry.Properties["Department"].Value != null) { DEPENDENCIA = dentry.Properties["Department"].Value.ToString(); } else { DEPENDENCIA = ""; }
                    if (dentry.Properties["streetAddress"].Value != null) { CALLEYNUMERO = dentry.Properties["streetAddress"].Value.ToString(); } else { CALLEYNUMERO = ""; }
                    if (dentry.Properties["colonia"].Value != null) { COLONIA = dentry.Properties["colonia"].Value.ToString(); } else { COLONIA = ""; }
                    if (dentry.Properties["postalCode"].Value != null) { CODIGOPOSTAL = dentry.Properties["postalCode"].Value.ToString(); } else { CODIGOPOSTAL = ""; }
                    if (dentry.Properties["l"].Value != null) { CIUDAD = dentry.Properties["l"].Value.ToString(); } else { CIUDAD = ""; }
                    if (dentry.Properties["st"].Value != null) { ESTADO = dentry.Properties["st"].Value.ToString(); } else { ESTADO = ""; }
                    if (dentry.Properties["CO"].Value != null) { PAIS = dentry.Properties["CO"].Value.ToString(); } else { PAIS = ""; }
                    if (dentry.Properties["edificio"].Value != null) { EDIFICIO = dentry.Properties["edificio"].Value.ToString(); } else { EDIFICIO = ""; }
                    if (dentry.Properties["piso"].Value != null) { PISO = dentry.Properties["piso"].Value.ToString(); } else { PISO = ""; }
                    if (dentry.Properties["telephoneNumber"].Value != null) { TELEFONOOFICINA = dentry.Properties["telephoneNumber"].Value.ToString(); } else { TELEFONOOFICINA = ""; }
                    if (dentry.Properties["mobile"].Value != null) { TELEFONOMOVIL = dentry.Properties["mobile"].Value.ToString(); } else { TELEFONOMOVIL = ""; }
                    if (dentry.Properties["homePhone"].Value != null) { TELEFONOPARTICULAR = dentry.Properties["homePhone"].Value.ToString(); } else { TELEFONOPARTICULAR = ""; }
                    if (dentry.Properties["facsimiletelephoneNumber"].Value != null) { FAC = dentry.Properties["facsimiletelephoneNumber"].Value.ToString(); } else { FAC = ""; }
                    if (dentry.Properties["info"].Value != null) { INFO = dentry.Properties["info"].Value.ToString(); } else { INFO = ""; }
                    if (dentry.Properties["wWWHomePage"].Value != null) { WEBPAGE = dentry.Properties["wWWHomePage"].Value.ToString(); } else { WEBPAGE = ""; }
                    if (dentry.Properties["pager"].Value != null) { PAGER = dentry.Properties["pager"].Value.ToString(); } else { PAGER = ""; }
                    if (dentry.Properties["memberOf"].Value != null) { MEMBEROF = dentry.Properties["memberOf"].Value.ToString(); } else { MEMBEROF = ""; }
                    if (dentry.Properties["ipPhone"].Value != null) { IPPHONE = dentry.Properties["ipPhone"].Value.ToString(); } else { IPPHONE = ""; }
                    // start 03/02/17
                    if (dentry.Properties["postOfficeBox"].Value != null) { FECHAALTA = dentry.Properties["postOfficeBox"].Value.ToString(); } else { FECHAALTA = ""; }
                    if (dentry.Properties["ExtensionName"].Value != null) { FECHABAJA = dentry.Properties["ExtensionName"].Value.ToString(); } else { FECHABAJA = ""; }
                    // end 03/02/17

                    /*telephoneAssistant*/
                    if (dentry.Properties["manager"].Value != null)
                    {
                        JEFEINMEDIATO = BuscarJefe_PorPath(dentry.Properties["manager"].Value.ToString());
                    }
                    else { JEFEINMEDIATO = ""; }

                    Encontrado = true;
                }
            }
            catch (Exception ex)
            {
                writer.Write(ex.Message);
            }
            dntry.Close();
            dntry.Dispose();
            deSearch.Dispose();
            if (!Encontrado) { writer.Write("<b>USUARIO NO ENCONTRADO</b>"); }
            return Encontrado;
        }


        public bool CargarInfo(string user)
        {
             samacaunt = user;
            bool Encontrado = false;
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + user + ") )";
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);
            try
            {
                foreach (SearchResult resEnt in deSearch.FindAll())
                {
                    DirectoryEntry dentry = resEnt.GetDirectoryEntry();
                    if (dentry.Properties["employeeID"].Value != null) { NOEMPLEADO = dentry.Properties["employeeID"].Value.ToString(); } else { NOEMPLEADO = ""; }
                    if (dentry.Properties["givenName"].Value != null) { NOMBREEMPLEADO = dentry.Properties["givenName"].Value.ToString(); } else { NOMBREEMPLEADO = ""; }
                    if (dentry.Properties["sn"].Value != null) { APELLIDOSEMPLEADO = dentry.Properties["sn"].Value.ToString(); } else { APELLIDOSEMPLEADO = ""; }
                    if (dentry.Properties["nivelcategoria"].Value != null) { CATEGORIA = dentry.Properties["nivelcategoria"].Value.ToString(); } else { CATEGORIA = ""; }
                    if (dentry.Properties["rfc"].Value != null) { RFC = dentry.Properties["rfc"].Value.ToString(); } else { RFC = ""; }
                    if (dentry.Properties["curp"].Value != null) { CURP = dentry.Properties["curp"].Value.ToString(); } else { CURP = ""; }
                    if (dentry.Properties["title"].Value != null) { PUESTO = dentry.Properties["title"].Value.ToString(); } else { PUESTO = ""; }
                    if (dentry.Properties["Company"].Value != null) { SECRETARIA = dentry.Properties["Company"].Value.ToString(); } else { SECRETARIA = ""; }
                    if (dentry.Properties["Department"].Value != null) { SUBSECRETARIA = dentry.Properties["Department"].Value.ToString(); } else { SUBSECRETARIA = ""; }
                    if (dentry.Properties["physicalDeliveryOfficeName"].Value != null) { DEPENDENCIA = dentry.Properties["physicalDeliveryOfficeName"].Value.ToString(); } else { DEPENDENCIA = ""; }//NUEVO
                    if (dentry.Properties["streetAddress"].Value != null) { CALLEYNUMERO = dentry.Properties["streetAddress"].Value.ToString(); } else { CALLEYNUMERO = ""; }
                    if (dentry.Properties["colonia"].Value != null) { COLONIA = dentry.Properties["colonia"].Value.ToString(); } else { COLONIA = ""; }
                    if (dentry.Properties["postalCode"].Value != null) { CODIGOPOSTAL = dentry.Properties["postalCode"].Value.ToString(); } else { CODIGOPOSTAL = ""; }
                    if (dentry.Properties["l"].Value != null) { CIUDAD = dentry.Properties["l"].Value.ToString(); } else { CIUDAD = ""; }
                    if (dentry.Properties["st"].Value != null) { ESTADO = dentry.Properties["st"].Value.ToString(); } else { ESTADO = ""; }
                    if (dentry.Properties["CO"].Value != null) { PAIS = dentry.Properties["CO"].Value.ToString(); } else { PAIS = ""; }
                    if (dentry.Properties["edificio"].Value != null) { EDIFICIO = dentry.Properties["edificio"].Value.ToString(); } else { EDIFICIO = ""; }
                    if (dentry.Properties["piso"].Value != null) { PISO = dentry.Properties["piso"].Value.ToString(); } else { PISO = ""; }
                    if (dentry.Properties["telephoneNumber"].Value != null) { TELEFONOOFICINA = dentry.Properties["telephoneNumber"].Value.ToString(); } else { TELEFONOOFICINA = ""; }
                    if (dentry.Properties["mobile"].Value != null) { TELEFONOMOVIL = dentry.Properties["mobile"].Value.ToString(); } else { TELEFONOMOVIL = ""; }
                    if (dentry.Properties["homePhone"].Value != null) { TELEFONOPARTICULAR = dentry.Properties["homePhone"].Value.ToString(); } else { TELEFONOPARTICULAR = ""; }
                    if (dentry.Properties["distinguishedName"].Value != null) { OU = dentry.Properties["distinguishedName"].Value.ToString(); } else OU = "";
                    if (dentry.Properties["msExchHomeServerName"].Value != null) { PUBLISHEDAT = dentry.Properties["msExchHomeServerName"].Value.ToString(); } else PublishedAt = "";
                    if (dentry.Properties["homeMDB"].Value != null) { MAILBOXSTORE = dentry.Properties["homeMDB"].Value.ToString(); } else MAILBOXSTORE = "";
                    if (dentry.Properties["facsimiletelephoneNumber"].Value != null) { FAC = dentry.Properties["facsimiletelephoneNumber"].Value.ToString(); } else FAC = "";
                    if (dentry.Properties["ipPhone"].Value != null) { IPPHONE = dentry.Properties["ipPhone"].Value.ToString(); } else IPPHONE = "";
                    if (dentry.Properties["wWWHomePage"].Value != null) { WEBPAGE = dentry.Properties["wWWHomePage"].Value.ToString(); } else WEBPAGE = "";
                    if (dentry.Properties["pager"].Value != null) { PAGER = dentry.Properties["pager"].Value.ToString(); } else PAGER = "";
                    if (dentry.Properties["info"].Value != null) { INFO = dentry.Properties["info"].Value.ToString(); } else INFO = "";
                    // start 03/02/17
                    if (dentry.Properties["postOfficeBox"].Value != null) { FECHAALTA = dentry.Properties["postOfficeBox"].Value.ToString(); } else { FECHAALTA = ""; }
                    if (dentry.Properties["ExtensionName"].Value != null) { FECHABAJA = dentry.Properties["ExtensionName"].Value.ToString(); } else { FECHABAJA = ""; }
                    // end 03/02/17

                    //if (dentry.Properties["memberOf"].Value != null) { MEMBEROF = dentry.Properties["memberOf"].Value.ToString(); } else MEMBEROF = "";
                    if (dentry.Properties["manager"].Value != null)
                    {
                        JEFEINMEDIATO = BuscarJefe_PorPath(dentry.Properties["manager"].Value.ToString());
                    }
                    else { JEFEINMEDIATO = ""; }
                    DateTime dtime = DateTime.MinValue;
                    if (dentry.Properties["lastLogon"].Value != null)
                    {
                        LargeInteger li = (LargeInteger)dentry.Properties["lastLogon"][0];
                        dtime = GetDateTimeFromLargeInteger(li);
                    }
                    LASTLOGON = dtime;
                    Encontrado = true;
                    dentry.Dispose();
                }
            }
            catch (Exception ex)
            {
                Stowrite = ex.Message;
                Encontrado = false;
            }

            dntry.Dispose();
            deSearch.Dispose();
            try
            {
                LASTLOGON = getRealLastLogon(LASTLOGON);
            }
            catch (Exception ex) { Stowrite = "No fue posible obtener la ultima fecha de valdiacion real. Detalles: " + ex.Message; }


            return Encontrado;
        }

        //Agregado 17.05.17
        public string CargarInfos(string user)
        {
            samacaunt = user;
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            //deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + user + ") )";
            deSearch.Filter = "(&(objectClass=user) (displayName=*" + user + "*))";
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);
            string tbl ="";
            tbl = "<thead><tr><th>Usuario</th><td>Nombre</td><td>Id</td></tr></thead><tbody>";
            try
            {
                foreach (SearchResult resEnt in deSearch.FindAll())
                {
                    tbl += "<tr>"; 
                    DirectoryEntry dentry = resEnt.GetDirectoryEntry();
                    if (dentry.Properties["sAMAccountName"].Value != null) { tbl += "<td><a class='id-empleado' id-emp='" + dentry.Properties["sAMAccountName"].Value.ToString() + "' nombre='" + dentry.Properties["givenName"].Value.ToString() +" "+ dentry.Properties["sn"].Value.ToString() + "'><i style='color:black;' class='fa fa-plus fa-3' aria-hidden='true'></i></a> " + dentry.Properties["sAMAccountName"].Value.ToString() + "</td>"; } else { tbl += "<td></td>"; }
                    if (dentry.Properties["givenName"].Value != null) { tbl +="<td>" + dentry.Properties["givenName"].Value.ToString() +" "+ dentry.Properties["sn"].Value.ToString()+ "</td>"; } else { tbl += "<td></td>"; }
                    if (dentry.Properties["employeeID"].Value != null) { tbl += "<td>" + dentry.Properties["employeeID"].Value.ToString() + "</td>"; } else { tbl += "<td></td>"; }

                    //if (dentry.Properties["sn"].Value != null) { APELLIDOSEMPLEADO = dentry.Properties["sn"].Value.ToString(); } else { APELLIDOSEMPLEADO = tbl += "<td></td>"; }
                    //if (dentry.Properties["nivelcategoria"].Value != null) { CATEGORIA = dentry.Properties["nivelcategoria"].Value.ToString(); } else { CATEGORIA = tbl += "<td></td>"; }
                    //if (dentry.Properties["rfc"].Value != null) { RFC = dentry.Properties["rfc"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["curp"].Value != null) { CURP = dentry.Properties["curp"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["title"].Value != null) { PUESTO = dentry.Properties["title"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["Company"].Value != null) { SECRETARIA = dentry.Properties["Company"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["Department"].Value != null) { DEPENDENCIA = dentry.Properties["Department"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["streetAddress"].Value != null) { CALLEYNUMERO = dentry.Properties["streetAddress"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["colonia"].Value != null) { COLONIA = dentry.Properties["colonia"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["postalCode"].Value != null) { CODIGOPOSTAL = dentry.Properties["postalCode"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["l"].Value != null) { CIUDAD = dentry.Properties["l"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["st"].Value != null) { ESTADO = dentry.Properties["st"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["CO"].Value != null) { PAIS = dentry.Properties["CO"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["edificio"].Value != null) { EDIFICIO = dentry.Properties["edificio"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["piso"].Value != null) { PISO = dentry.Properties["piso"].Value.ToString(); } else { tbl += "<td></td>"; }
                    //if (dentry.Properties["telephoneNumber"].Value != null) { TELEFONOOFICINA = dentry.Properties["telephoneNumber"].Value.ToString(); } else { TELEFONOOFICINA = ""; }
                    //if (dentry.Properties["mobile"].Value != null) { TELEFONOMOVIL = dentry.Properties["mobile"].Value.ToString(); } else { TELEFONOMOVIL = ""; }
                    //if (dentry.Properties["homePhone"].Value != null) { TELEFONOPARTICULAR = dentry.Properties["homePhone"].Value.ToString(); } else { TELEFONOPARTICULAR = ""; }
                    //if (dentry.Properties["distinguishedName"].Value != null) { OU = dentry.Properties["distinguishedName"].Value.ToString(); } else OU = "";
                    //if (dentry.Properties["msExchHomeServerName"].Value != null) { PUBLISHEDAT = dentry.Properties["msExchHomeServerName"].Value.ToString(); } else PublishedAt = "";
                    //if (dentry.Properties["homeMDB"].Value != null) { MAILBOXSTORE = dentry.Properties["homeMDB"].Value.ToString(); } else MAILBOXSTORE = "";
                    //if (dentry.Properties["facsimiletelephoneNumber"].Value != null) { FAC = dentry.Properties["facsimiletelephoneNumber"].Value.ToString(); } else FAC = "";
                    //if (dentry.Properties["ipPhone"].Value != null) { IPPHONE = dentry.Properties["ipPhone"].Value.ToString(); } else IPPHONE = "";
                    //if (dentry.Properties["wWWHomePage"].Value != null) { WEBPAGE = dentry.Properties["wWWHomePage"].Value.ToString(); } else WEBPAGE = "";
                    //if (dentry.Properties["pager"].Value != null) { PAGER = dentry.Properties["pager"].Value.ToString(); } else PAGER = "";
                    //if (dentry.Properties["info"].Value != null) { INFO = dentry.Properties["info"].Value.ToString(); } else INFO = "";
                    //// start 03/02/17
                    //if (dentry.Properties["postOfficeBox"].Value != null) { FECHAALTA = dentry.Properties["postOfficeBox"].Value.ToString(); } else { FECHAALTA = ""; }
                    //if (dentry.Properties["ExtensionName"].Value != null) { FECHABAJA = dentry.Properties["ExtensionName"].Value.ToString(); } else { FECHABAJA = ""; }
                    //// end 03/02/17

                    ////if (dentry.Properties["memberOf"].Value != null) { MEMBEROF = dentry.Properties["memberOf"].Value.ToString(); } else MEMBEROF = "";
                    //if (dentry.Properties["manager"].Value != null)
                    //{
                    //    JEFEINMEDIATO = BuscarJefe_PorPath(dentry.Properties["manager"].Value.ToString());
                    //}
                    //else { JEFEINMEDIATO = ""; }
                    //DateTime dtime = DateTime.MinValue;
                    //if (dentry.Properties["lastLogon"].Value != null)
                    //{
                    //    LargeInteger li = (LargeInteger)dentry.Properties["lastLogon"][0];
                    //    dtime = GetDateTimeFromLargeInteger(li);
                    //}
                    
                    //LASTLOGON = dtime;
                    dentry.Dispose();
                    tbl += "</tr>";
                }
                tbl += "</tbody>";
            }
            catch (Exception ex)
            {
                Stowrite = ex.Message;
            }

            dntry.Dispose();
            deSearch.Dispose();


            return tbl;
        }


        private DateTime getRealLastLogon(DateTime LAST)
        {
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry("LDAP://10.144.0.18", info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            DirectorySearcher ds = new DirectorySearcher(dntry);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + SAMACOUNT + ") )";

            DateTime dtime = DateTime.MinValue;
            foreach (SearchResult resEnt in deSearch.FindAll())
            {
                DirectoryEntry de = resEnt.GetDirectoryEntry();
                if (de.Properties["lastLogon"].Value != null)
                {
                    LargeInteger li = (LargeInteger)de.Properties["lastLogon"][0];
                    dtime = GetDateTimeFromLargeInteger(li);
                }
            }
            if (dtime > LAST) { return dtime; } else { return LAST; }

        }
        public UsuarioAD()
        {
            //Constructor Sin Atributos
            //

        }
        public void GuardarCambios(string user)
        {
            string strJefe = "";
            if (JEFEINMEDIATO != "")
            {
                strJefe = BuscarJefe_PorCN(JEFEINMEDIATO);
                if (strJefe == "") { Stowrite = "No fue posible encontrar al usuario inscrito en el campo Jefe"; }
            }

            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + user + ") )";
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);
            
            try
            {


                   dntry = deSearch.FindOne().GetDirectoryEntry();


                    GuardaPropiedad(ref dntry, "employeeID", NOEMPLEADO);
                    GuardaPropiedad(ref dntry, "givenName", NOMBREEMPLEADO);
                    GuardaPropiedad(ref dntry, "sn", APELLIDOSEMPLEADO);
                    GuardaPropiedad(ref dntry, "displayName", NOMBREEMPLEADO + " " + APELLIDOSEMPLEADO);
                    GuardaPropiedad(ref dntry, "nivelcategoria", Categoria);
                    GuardaPropiedad(ref dntry, "rfc", RFC);
                    GuardaPropiedad(ref dntry, "curp", CURP);
                    GuardaPropiedad(ref dntry, "title", PUESTO);
                    GuardaPropiedad(ref dntry, "description", PUESTO);
                    GuardaPropiedad(ref dntry, "Company", SECRETARIA);
                    GuardaPropiedad(ref dntry, "Department", SUBSECRETARIA);
                    GuardaPropiedad(ref dntry, "physicalDeliveryOfficeName", DEPENDENCIA);
                    GuardaPropiedad(ref dntry, "streetAddress", CALLEYNUMERO);
                    GuardaPropiedad(ref dntry, "colonia", COLONIA);
                    GuardaPropiedad(ref dntry, "postalCode", CODIGOPOSTAL);
                    GuardaPropiedad(ref dntry, "l", CIUDAD);
                    GuardaPropiedad(ref dntry, "st", ESTADO);
                    GuardaPropiedad(ref dntry, "facsimiletelephoneNumber", FAC);
                    string spais = "Mexico"; string sccode = "484"; string sc = "MX";
                    dntry.Properties["CO"].Value = spais;
                    dntry.Properties["countryCode"].Value = sccode;
                    dntry.Properties["c"].Value = sc;

                    GuardaPropiedad(ref dntry, "edificio", EDIFICIO);
                    GuardaPropiedad(ref dntry, "piso", PISO);
                    GuardaPropiedad(ref dntry, "telephoneNumber", TELEFONOOFICINA);
                    GuardaPropiedad(ref dntry, "mobile", TELEFONOMOVIL);
                    GuardaPropiedad(ref dntry, "homePhone", TELEFONOPARTICULAR);
                   GuardaPropiedad(ref dntry, "wWWHomePage", WEBPAGE);
                    GuardaPropiedad(ref dntry, "pager", PAGER);
                    GuardaPropiedad(ref dntry, "info", INFO);
                    GuardaPropiedad(ref dntry, "ipPhone", IPPHONE);
                    // start 03/02/17
                    GuardaPropiedad(ref dntry, "postOfficeBox", FECHAALTA);
                    GuardaPropiedad(ref dntry, "ExtensionName", FECHABAJA);
                    // end 03/02/17
                    GuardaPropiedad(ref dntry, "manager", strJefe.Replace(info_Temp.GetLDAP_URL + "/", ""));
                    dntry.CommitChanges();
               

            }
            catch (Exception ex) { throw new Exception("No fue posible actualizar la informacion. Detalle: " + ex.Message); }
            finally { dntry.Dispose(); deSearch.Dispose(); }
        }

        public void GuardarCambios200617(string user)
        {
            string strJefe = "";
            if (JEFEINMEDIATO != "")
            {
                strJefe = BuscarJefe_PorCN(JEFEINMEDIATO);
                if (strJefe == "") { Stowrite = "No fue posible encontrar al usuario inscrito en el campo Jefe"; }
            }
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user) (sAMAccountName=" + user + ") )";
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);

            try
            {


                dntry = deSearch.FindOne().GetDirectoryEntry();

                GuardaPropiedad(ref dntry, "employeeID", NOEMPLEADO);
                GuardaPropiedad(ref dntry, "givenName", NOMBREEMPLEADO);
                GuardaPropiedad(ref dntry, "sn", APELLIDOSEMPLEADO);
                GuardaPropiedad(ref dntry, "displayName", NOMBREEMPLEADO + " " + APELLIDOSEMPLEADO);
                GuardaPropiedad(ref dntry, "nivelcategoria", "CO - Coordinador");//Categoria
                GuardaPropiedad(ref dntry, "rfc", RFC);
                GuardaPropiedad(ref dntry, "curp", CURP);
                GuardaPropiedad(ref dntry, "title", PUESTO);
                GuardaPropiedad(ref dntry, "description", PUESTO);
                GuardaPropiedad(ref dntry, "Company", SECRETARIA);
                GuardaPropiedad(ref dntry, "Department", "Subsecretaria de Tecnologías");
                GuardaPropiedad(ref dntry, "physicalDeliveryOfficeName", DEPENDENCIA);//OFICINA/dependencia
                GuardaPropiedad(ref dntry, "streetAddress", CALLEYNUMERO);
                GuardaPropiedad(ref dntry, "colonia", COLONIA);
                GuardaPropiedad(ref dntry, "postalCode", CODIGOPOSTAL);
                GuardaPropiedad(ref dntry, "l", CIUDAD);
                GuardaPropiedad(ref dntry, "st", ESTADO);
                GuardaPropiedad(ref dntry, "facsimiletelephoneNumber", FAC);
                string spais = "Mexico"; string sccode = "484"; string sc = "MX";
                dntry.Properties["CO"].Value = spais;
                dntry.Properties["countryCode"].Value = sccode;
                dntry.Properties["c"].Value = sc;

                GuardaPropiedad(ref dntry, "edificio", EDIFICIO);
                GuardaPropiedad(ref dntry, "piso", PISO);
                GuardaPropiedad(ref dntry, "telephoneNumber", TELEFONOOFICINA);
                //GuardaPropiedad(ref dntry, "mobile", TELEFONOMOVIL);
                //GuardaPropiedad(ref dntry, "homePhone", TELEFONOPARTICULAR);
                GuardaPropiedad(ref dntry, "wWWHomePage", WEBPAGE);// tipo de cuenta
                GuardaPropiedad(ref dntry, "pager", PAGER);
                GuardaPropiedad(ref dntry, "info", INFO);//tipo de empleado
                GuardaPropiedad(ref dntry, "ipPhone", IPPHONE);
                // start 03/02/17
                GuardaPropiedad(ref dntry, "postOfficeBox", FECHAALTA);
                //GuardaPropiedad(ref dntry, "ExtensionName", FECHABAJA);
                //end 03/02/17
                GuardaPropiedad(ref dntry, "manager", strJefe.Replace(info_Temp.GetLDAP_URL + "/", ""));
                dntry.CommitChanges();


            }
            catch (Exception ex) { throw new Exception("No fue posible actualizar la informacion. Detalle: " + ex.Message); }
            finally { dntry.Dispose(); deSearch.Dispose(); }
        }

        private void GuardaPropiedad(ref DirectoryEntry de, string propiedad, string valor)
        {
            if (valor != "")
            {
                if (de.Properties[propiedad].Value != null)
                {
                    if (de.Properties[propiedad].Value.ToString() != valor)
                    {
                        de.Properties[propiedad].Value = valor;
                    }
                }
                else { de.Properties[propiedad].Value = valor; }

            }
            else
            {
                if (de.Properties[propiedad].Value == null)
                {
                    return;
                }
                else
                {
                    de.Properties[propiedad].Value = null;
                }
            }
        }

        //
        //Busca al Jefe atraves del CN y devuelve el Path
        //

        private string BuscarJefe_PorCN(string JEFE)
        {
            string strtoReturn = "";
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Filter = "(&(objectClass=user)(CN=" + JEFE + "))";
            try
            {
                SearchResult results = deSearch.FindOne();
                if (results != null)
                {
                    strtoReturn = results.Path;
                }
            }
            catch (Exception ex) { throw new Exception("No fue posible encontrar el Jefe inmediato. Causa: " + ex.Message); }
            finally { dntry.Dispose(); deSearch.Dispose(); }
            return strtoReturn;
        }

        //
        //
        //Busca el jefe a traves del path y devuelve el  CN
        //
        //
        private string BuscarJefe_PorPath(string Jefe_Path)
        {
            string strtoReturn = "";
            CSInfo info_Temp = new CSInfo();
            DirectoryEntry dntry = new DirectoryEntry(info_Temp.GetLDAP_URL + "/" + Jefe_Path, info_Temp.GetLDAP_User, info_Temp.GetLDAP_Pass);
            dntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher deSearch = new DirectorySearcher();
            deSearch.SearchRoot = dntry;
            deSearch.Sort = new SortOption("displayName", System.DirectoryServices.SortDirection.Ascending);

            try
            {
                dntry = deSearch.FindOne().GetDirectoryEntry();
                strtoReturn = dntry.Properties["cn"].Value.ToString();
            }
            catch (Exception ex) { throw new Exception("No fue posible encontrar el Jefe inmediato. Causa: " + ex.Message); }
            finally { dntry.Dispose(); deSearch.Dispose(); }
            return strtoReturn;
        }

        public static DateTime GetDateTimeFromLargeInteger(IADsLargeInteger largeIntValue)
        {
            //
            // Convert large integer to int64 value
            //
            long int64Value = (long)((uint)largeIntValue.LowPart +
                     (((long)largeIntValue.HighPart) << 32));

            //
            // Return the DateTime in utc
            //
            return DateTime.FromFileTimeUtc(int64Value);
        }
    }
    }
