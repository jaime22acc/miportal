﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Net;
using System.Data;
using System.Text;
using System.Globalization;
using System.Configuration;
using System.Web.Services.Protocols;
using System.Data.Odbc;

namespace Emplea
{
    public class utils
    {
        public static bool esnulo(string campo)
            {
                if (campo == null || string.IsNullOrEmpty(campo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


            public static void procesarMensaje(ref StringBuilder msg, string nombreDeColumna)
            {
                string separador = ";";
                if (msg.Length == 0)
                {
                    msg.Append("Nulos:[" + nombreDeColumna + "]");
                }
                else
                {
                    msg.Append(separador + " [" + nombreDeColumna + "]");
                }
            }

           

           
        
            public static bool IsDate(Object obj)
            {
                string strDate = obj.ToString();
                try
                {
                    string dia = strDate.Substring(4, 2);
                    string anio = strDate.Substring(0, 4);
                    string mes = strDate.Substring(6, 2);
                    string fecha = anio + "-" + dia + "-" + mes;
                    DateTime dt = DateTime.Parse(fecha);
                    if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                        return true;
                    return false;
                }
                catch
                {
                    return false;
                }
            }
            /// <summary>
            /// Ejecuta un sp y regresa el resultado de la ejecucion como un int
            /// </summary>
            /// <param name="connString">ConnectionString</param>
            /// <param name="cmdType">Tipo de Comando</param>
            /// <param name="spName">Nombre del SP o Comando de Sql</param>
            /// <param name="sqlParams">Parametros del store procedure</param>
            /// <returns></returns>
    

        public string PrimeraLetraMayuscula(string cadena)
        {
            
            
         //  string palabra, primera;
         //  cadena = ""; 
         //  primera = cadena[0].ToString();//Aqui guardo la 1ra letra

         //  primera = primera.ToUpper();//Covierto a mayúscula

         //  for (int i = 1; i < tbCadena.Text.Length; i++) //en etes ciclo se coge la palabra menos la 1ra letra
         //  {
         //    cadena = cadena + tbCadena.Text[i];
         //  }

         //    cadena = primera + cadena;

            return cadena;
        }

       
        public static string camelCase(string str)
        {

            str = str.ToLower();
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            str = textInfo.ToTitleCase(str);
            return str;
        }

       
    }
}