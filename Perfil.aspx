﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="Emplea.Perfil" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset='utf-8'>

      <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="Scripts/img/favicon.ico" type="image/vnd.microsoft.icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="follow, index">
    <meta name="generator" content="Gobierno del Estado de Nuevo León">
    <title> Perfil | Gobierno del Estado de Nuevo León</title>
   <link href="Scripts/css/bootstrap.min.css" rel="stylesheet">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/dropdowns/">    
  
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>  
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="Scripts/css/calendar.css">

    
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link href="Scripts/js/jquery-ui-1.13.1.custom/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <style>
    @import url("Scripts/css/bootstrap-grid.css");
    @import url("Scripts/css/style.css");
    </style>
    <link href="Scripts/css/dropdowns.css" rel="stylesheet">
    <link href="Scripts/css/bootstrap.min.css" rel="stylesheet">
    <link href="Scripts/css/font-awesome.min.css" rel="stylesheet">

    <style>
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap');
    </style>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700;800;900&display=swap');
    </style>
    <style>::-webkit-scrollbar {
      width: 8px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
    }
    
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    } @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");</style>
    <link href="Scripts/PNotify/pnotify.custom.css" media="all" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="Scripts/PNotify/pnotify.custom.js"></script>
</head>
    <body className='snippet-body'>
    <body id="body-pd" class="page page-capital-humano sesion-activa">
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search">
          <input type="search" class="form-control" placeholder="Buscar..." aria-label="Search">
        </form>

          <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <div class="datos-user">                
                 <div class="name"> <p style="font-family: 'Century Gothic'" id="lblUsuario" runat="server"></p></div>
                <div class="dependencia"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblDependencia" runat="server"></p></div>
               
            </div>
            <img src="Scripts/img/user.jpg" alt="mdo" width="32" height="32" class="rounded-circle">
          </a>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
            <div class="num-empleado">
                <div class="num-empleado-label">Bienvenido(a)</div>
                <div class="num-empleado-codigo"></div>
            </div>
            <div class="usuario-perfil-controles">
                <%--<button class="btn green">Activo</button>
                <button class="btn brown">Editar Perfil</button>--%>
            </div>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="Perfil.aspx"><img src="Scripts/img/Mi_cuenta.svg">  Mi Cuenta</a></li>
            <li><a class="dropdown-item" href="Login.aspx"><img src="Scripts/img/cerrar_sesion.svg">  Cerrar Sesión</a></li>
          </ul>
        </div>
        </header>
    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> 
                <a href="Default.aspx" class="nav_logo"> <div class="logo_menu"></div></a>
                 <div class="nav_list"> 
                           <a href="Default.aspx" class="nav_link"> <img src="Scripts/img/inicio.svg"> <span class="nav_name">Inicio</span> </a> 
                        <a href="NominaR.aspx" class="nav_link active"> <img src="Scripts/img/LS_Nomina.svg"> <span class="nav_name">Nómina</span> </a>
                        <a href="https://sa.nl.gob.mx/Rhservicios" class="nav_link" target="_blank"> <img src="Scripts/img/A_Educacion.svg" > <span class="nav_name">Apoyo Escolar Coporaciones Policiacas</span> </a>
                        <a href="https://sa.nl.gob.mx/Rhservicios" class="nav_link" target="_blank"> <img src="Scripts/img/Ahorro_Retiro.svg"> <span class="nav_name">Fondo de ahorro para el retiro<div class="prox">Próximamente</div></span> </a> 
                        <a href="http://profesionalizacion.nl.gob.mx/index.html" target="_blank" class="nav_link"> <img src="Scripts/img/Cursos_Online.svg" > <span class="nav_name">Cursos en línea</span> </a>   
                        <a href="https://sa.nl.gob.mx/Rhservicios/Vacacion.aspx" class="nav_link" target="_blank"> <img src="Scripts/img/LS_Descuentos.svg" > <span class="nav_name">Vacaciones</span> </a> 
                        <a href="Patrimonial.aspx" class="nav_link"> <img src="Scripts/img/Informe_Declaracion_Patrimonial.svg"> <span class="nav_name">Informes de ingreso para declaración patrimonial</span> </a> 
                        <a href="Forma37.aspx" class="nav_link"> <img src="Scripts/img/LS_C_Retencion.svg"> <span class="nav_name">Constancias de sueldos y retenciones (Forma 37)</span> </a> 
                        <a href="Comparte.aspx" class="nav_link"> <img src="Scripts/img/Boletin.svg"> <span class="nav_name">Boletín ‘Comparte’ </span> </a> 
                        <a href="Perfil.aspx" class="nav_link"> <img src="Scripts/img/Mi_cuenta.svg"> <span class="nav_name">Mi Cuenta</span> </a> 
                   </div>
                <div class="menu-mensaje">
                    <img src="Scripts/img/Lupa.png">
                    <h4>¿Necesitas ayuda?</h4>
                    <p>Marca al 123 desde tu teléfono de oficina o escríbenos</p>

                    <p><strong>cast@nuevoleon.gob.mx</strong></p>
                </div>
            </div>
        </nav>
    </div>
    <!--Container Main start-->
    <div class="content-nomina">
        <h4>Mi cuenta</h4>
        <div class="border-small"></div>
        <br>
        <div class="main-container container--wide container">
            <div class="col-12 col-md-12 izq perfil">
                <div class="row">
                    <div class="col-12">
                        <div class="encabezado">Perfil </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Nombre completo</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblNombre" runat="server"></p></div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Número de empleado</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblNoEmpleado" runat="server"></p></div>
                </div><div class="row">
                    <div class="col-3 col-md-3 label">Correo eléctronico</div>
                    <div class="col-6 col-md-6 value">
                        <div class="form-group">
                            <input type="email" class="form-control" id="txtEmail" aria-describedby="emailHelp" placeholder="Email" >
                        </div>
                    </div>
                  
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Número de teléfono</div>
                    <div class="col-6 col-md-6 value">
                        <div class="form-group">
                            <input type="text" class="form-control" id="txtTelefono" aria-describedby="telHelp" placeholder="Número de teléfono" >
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Extensión</div>
                    <div class="col-6 col-md-6 value">
                        <div class="form-group">
                            <input type="text" class="form-control" id="txtExtension" aria-describedby="extHelp" placeholder="Extensión" >
                        </div>
                    </div>
                    <div class="col-3 col-md-3">
                        <div class="usuario-perfil-controles">
                            
                            <button class="btn black" id="btnActualizar">Actualizar Perfil</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-12 col-md-12 izq perfil datos">
                <div class="row">
                    <div class="col-12">
                        <div class="encabezado">Datos del colaborador</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Tipo de contrato</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblContrato" runat="server"></p></div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Fecha de ingreso</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblFechaIngreso" runat="server"></p></div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Secretaría</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblSecretaria" runat="server"></p></div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Dependencia</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblPresupuestal" runat="server"></p></div>
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Lugar de trabajo</div>
                    <div class="col-6 col-md-6 value"> 
                        <p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblLugarTrabajo" runat="server"></p>

                    </div>
                 
                </div>
                <div class="row">
                    <div class="col-3 col-md-3 label">Reloj Checador</div>
                    <div class="col-6 col-md-6 value"><p style="color: #9fa2a3; font-family: 'Century Gothic'" id="lblDispositivo" runat="server"></p></div>
                </div>
            </div>
        </div>
        
    </div>
    <!--Container Main end-->
    <footer class="blue">
        <div class="row">
            <div class="col-12 col-xl-2 col-md-12">
                <div class="region region-footer-logo">
                    <img class="footer_img" src="Scripts/img/logo-escudo-footer.svg" alt="nuevo-leon" title="Nuevo León">
                </div>
            </div>
            <div class="col-12 col-xl-5 col-md-8 margin-auto">
               <h3>Servicios para colaboradores</h3>
		      <div class="col-12 col-xl-6 col-md-6 izq">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="NominaR.aspx" title="">Nómina</a></li>
					<li class="leaf"><a  href="https://sa.nl.gob.mx/Rhservicios" target="_blank" title="">Apoyo Escolar SSP</a></li>
					<li class="leaf"><a href="https://www.isssteleon.gob.mx/afiliado/solicitud_pcp.html" target="_blank" title="ISSSTELEON">Préstamos</a></li>
					<%--<li class="leaf"><a href="#" title="">Prestaciones</a></li>--%>
					<li class="leaf"><a href="https://sa.nl.gob.mx/Rhservicios" target="_blank" title="RHSERVICIOS">Vacaciones </a></li>
					<%--<li class="last"><a href="#" title="">Refrendo</a></li>--%>
				</ul>
		      </div>
		      <div class="col-12 col-xl-6 col-md-6 der">
		      	<ul class="menu">
		      		<li class="first leaf"><a href="Forma37.aspx" title="">Sueldos y retenciones</a></li>
					<li class="leaf"><a href="Patrimonial.aspx" title="">Declaración patrimonial</a></li>
					<%--<li class="leaf"><a href="#" title="">Constancia laboral</a></li>--%>
					<li class="leaf"><a href="http://profesionalizacion.nl.gob.mx/" target="_blank" title="">Capacitación</a></li>
					<%--<li class="leaf"><a href="#" title="">Un servicio más</a></li>
					<li class="last"><a href="#" title="">Otro más</a></li>--%>
				</ul>
		      </div>
            </div>
            <div class="col-12 col-xl-2 col-md-8 margin-auto">
                <%--<h3>Atención y soporte</h3>--%>
                <div class="col-12 col-xl-12 der">
                   <%-- <p>¿Tienes alguna solicitud de mantenimiento o requieres apoyo tecnológico?</p>
                    <p>Contáctanos y estaremos contigo para ayudarte</p>--%>
                </div>
            </div>
            <div class="col-12 col-xl-3 col-md-8 margin-auto">
                <h3></h3>
                <div class="col-12 col-xl-12">
                    <img class="footer_img" src="Scripts/img/logo_administracion.png" alt="administracion" title="Administración">
                </div>
            </div>
            <div class="social der">
                <div class="der">
                     <div class="facebook alineado"><a href="https://www.facebook.com/gobiernonuevoleon/"><img src="Scripts/img/Facebook.svg"></a></div>
		    		<div class="instagram alineado"><a href="https://www.instagram.com/nuevoleonmx"><img src="Scripts/img/Instagram.svg"></a></div>
		    		<div class="youtube alineado"><a href="https://www.youtube.com/user/GobiernoNuevoLeon"><img src="Scripts/img/Youtube.svg"></a></div>
		    
                </div>
            </div>
            <div class="col-12 center">
                <h5>Gobierno del Estado de Nuevo León</h5>
            </div>
        </div>
    </footer>
    <footer class="white">
        <div class="alineado text-dorado">SECRETARÍA DE ADMINISTRACIÓN</div>
        <div class="alineado"><img src="Scripts/img/footer_leon.svg"></div>
        <div class="alineado text-dorado">UN NUEVO NUEVO LEÓN</div>
    </footer>
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    
    <script type='text/javascript'>document.addEventListener("DOMContentLoaded", function(event) {
   
        const showNavbar = (toggleId, navId, bodyId, headerId) =>{
        const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        bodypd = document.getElementById(bodyId),
        headerpd = document.getElementById(headerId)

        // Validate that all variables exist
        if(toggle && nav && bodypd && headerpd){
        toggle.addEventListener('click', ()=>{
        // show navbar
        nav.classList.toggle('show')
        // change icon
        toggle.classList.toggle('bx-x')
        // add padding to body
        bodypd.classList.toggle('body-pd')
        // add padding to header
        headerpd.classList.toggle('body-pd')
        })
        }
        }

        showNavbar('header-toggle','nav-bar','body-pd','header')

        /*===== LINK ACTIVE =====*/
        const linkColor = document.querySelectorAll('.nav_link')

        function colorLink(){
        if(linkColor){
        linkColor.forEach(l=> l.classList.remove('active'))
        this.classList.add('active')
        }
        }
        linkColor.forEach(l=> l.addEventListener('click', colorLink))

         // Your code to run since DOM is loaded and ready
        });
    </script>
    <script type='text/javascript'>var myLink = document.querySelector('a[href="#"]');
    myLink.addEventListener('click', function(e) {
      e.preventDefault();
    });</script>
                  
           
    </body>
</html>
